/**
 * RunBookFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class RunBookFilter  implements java.io.Serializable {
    private java.lang.String uprocName;

    private java.lang.String taskName;

    private java.lang.String sessionName;

    private java.lang.String application;

    private java.lang.String muName;

    private java.lang.String submissionAccount;

    private java.lang.String queueName;

    private java.lang.String severity;

    public RunBookFilter() {
    }

    public RunBookFilter(
           java.lang.String uprocName,
           java.lang.String taskName,
           java.lang.String sessionName,
           java.lang.String application,
           java.lang.String muName,
           java.lang.String submissionAccount,
           java.lang.String queueName,
           java.lang.String severity) {
           this.uprocName = uprocName;
           this.taskName = taskName;
           this.sessionName = sessionName;
           this.application = application;
           this.muName = muName;
           this.submissionAccount = submissionAccount;
           this.queueName = queueName;
           this.severity = severity;
    }


    /**
     * Gets the uprocName value for this RunBookFilter.
     * 
     * @return uprocName
     */
    public java.lang.String getUprocName() {
        return uprocName;
    }


    /**
     * Sets the uprocName value for this RunBookFilter.
     * 
     * @param uprocName
     */
    public void setUprocName(java.lang.String uprocName) {
        this.uprocName = uprocName;
    }


    /**
     * Gets the taskName value for this RunBookFilter.
     * 
     * @return taskName
     */
    public java.lang.String getTaskName() {
        return taskName;
    }


    /**
     * Sets the taskName value for this RunBookFilter.
     * 
     * @param taskName
     */
    public void setTaskName(java.lang.String taskName) {
        this.taskName = taskName;
    }


    /**
     * Gets the sessionName value for this RunBookFilter.
     * 
     * @return sessionName
     */
    public java.lang.String getSessionName() {
        return sessionName;
    }


    /**
     * Sets the sessionName value for this RunBookFilter.
     * 
     * @param sessionName
     */
    public void setSessionName(java.lang.String sessionName) {
        this.sessionName = sessionName;
    }


    /**
     * Gets the application value for this RunBookFilter.
     * 
     * @return application
     */
    public java.lang.String getApplication() {
        return application;
    }


    /**
     * Sets the application value for this RunBookFilter.
     * 
     * @param application
     */
    public void setApplication(java.lang.String application) {
        this.application = application;
    }


    /**
     * Gets the muName value for this RunBookFilter.
     * 
     * @return muName
     */
    public java.lang.String getMuName() {
        return muName;
    }


    /**
     * Sets the muName value for this RunBookFilter.
     * 
     * @param muName
     */
    public void setMuName(java.lang.String muName) {
        this.muName = muName;
    }


    /**
     * Gets the submissionAccount value for this RunBookFilter.
     * 
     * @return submissionAccount
     */
    public java.lang.String getSubmissionAccount() {
        return submissionAccount;
    }


    /**
     * Sets the submissionAccount value for this RunBookFilter.
     * 
     * @param submissionAccount
     */
    public void setSubmissionAccount(java.lang.String submissionAccount) {
        this.submissionAccount = submissionAccount;
    }


    /**
     * Gets the queueName value for this RunBookFilter.
     * 
     * @return queueName
     */
    public java.lang.String getQueueName() {
        return queueName;
    }


    /**
     * Sets the queueName value for this RunBookFilter.
     * 
     * @param queueName
     */
    public void setQueueName(java.lang.String queueName) {
        this.queueName = queueName;
    }


    /**
     * Gets the severity value for this RunBookFilter.
     * 
     * @return severity
     */
    public java.lang.String getSeverity() {
        return severity;
    }


    /**
     * Sets the severity value for this RunBookFilter.
     * 
     * @param severity
     */
    public void setSeverity(java.lang.String severity) {
        this.severity = severity;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunBookFilter)) return false;
        RunBookFilter other = (RunBookFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uprocName==null && other.getUprocName()==null) || 
             (this.uprocName!=null &&
              this.uprocName.equals(other.getUprocName()))) &&
            ((this.taskName==null && other.getTaskName()==null) || 
             (this.taskName!=null &&
              this.taskName.equals(other.getTaskName()))) &&
            ((this.sessionName==null && other.getSessionName()==null) || 
             (this.sessionName!=null &&
              this.sessionName.equals(other.getSessionName()))) &&
            ((this.application==null && other.getApplication()==null) || 
             (this.application!=null &&
              this.application.equals(other.getApplication()))) &&
            ((this.muName==null && other.getMuName()==null) || 
             (this.muName!=null &&
              this.muName.equals(other.getMuName()))) &&
            ((this.submissionAccount==null && other.getSubmissionAccount()==null) || 
             (this.submissionAccount!=null &&
              this.submissionAccount.equals(other.getSubmissionAccount()))) &&
            ((this.queueName==null && other.getQueueName()==null) || 
             (this.queueName!=null &&
              this.queueName.equals(other.getQueueName()))) &&
            ((this.severity==null && other.getSeverity()==null) || 
             (this.severity!=null &&
              this.severity.equals(other.getSeverity())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUprocName() != null) {
            _hashCode += getUprocName().hashCode();
        }
        if (getTaskName() != null) {
            _hashCode += getTaskName().hashCode();
        }
        if (getSessionName() != null) {
            _hashCode += getSessionName().hashCode();
        }
        if (getApplication() != null) {
            _hashCode += getApplication().hashCode();
        }
        if (getMuName() != null) {
            _hashCode += getMuName().hashCode();
        }
        if (getSubmissionAccount() != null) {
            _hashCode += getSubmissionAccount().hashCode();
        }
        if (getQueueName() != null) {
            _hashCode += getQueueName().hashCode();
        }
        if (getSeverity() != null) {
            _hashCode += getSeverity().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunBookFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "runBookFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("application");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "application"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("muName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "muName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "submissionAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queueName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "queueName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("severity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "severity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

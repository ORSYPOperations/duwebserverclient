/**
 * DuWebServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class DuWebServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.orsyp.duws.DuWebService_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[54];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
        _initOperationDesc6();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("forceCompleteLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListUproc");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocFilter"), com.orsyp.duws.UprocFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocItem"));
        oper.setReturnClass(com.orsyp.duws.UprocItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), com.orsyp.duws.EventId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListSession");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionFilter"), com.orsyp.duws.SessionFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionItem"));
        oper.setReturnClass(com.orsyp.duws.SessionItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"));
        oper.setReturnClass(com.orsyp.duws.Launch.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"), com.orsyp.duws.Launch.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setReturnClass(com.orsyp.duws.LaunchId.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("restartEngine");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), com.orsyp.duws.Engine.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "eventFilter"), com.orsyp.duws.EventFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventItem"));
        oper.setReturnClass(com.orsyp.duws.EventItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRunBooks");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "runBookFilter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "runBookFilter"), com.orsyp.duws.RunBookFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "runBook"));
        oper.setReturnClass(com.orsyp.duws.RunBook[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "runbooks"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPreviousLaunches");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "previousLaunch"));
        oper.setReturnClass(com.orsyp.duws.PreviousLaunch[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "previousLaunches"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addLaunchFromTask");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), com.orsyp.duws.TaskId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setReturnClass(com.orsyp.duws.LaunchId.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("stopQueue");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), com.orsyp.duws.Queue.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("logout");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "token"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("enableLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListMU");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "muFilter"), com.orsyp.duws.MuFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "muItem"));
        oper.setReturnClass(com.orsyp.duws.MuItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "muList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("startEngine");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), com.orsyp.duws.Engine.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getExecutionLog");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionLog"));
        oper.setReturnClass(com.orsyp.duws.ExecutionLog.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionLog"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "execution"));
        oper.setReturnClass(com.orsyp.duws.Execution.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "execution"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), com.orsyp.duws.EventId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "status"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "step"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("resetQueue");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), com.orsyp.duws.Queue.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"), com.orsyp.duws.Launch.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRunBookExternalFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "runBook"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "runBook"), com.orsyp.duws.RunBook.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        oper.setReturnClass(byte[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "fileDataHandler"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createOutageWindow");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "outage"), org.apache.axis.description.ParameterDesc.INOUT, new javax.xml.namespace.QName("http://duws.orsyp.com", "outageWindow"), com.orsyp.duws.OutageWindow.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchFilter"), com.orsyp.duws.LaunchFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchItem"));
        oper.setReturnClass(com.orsyp.duws.LaunchItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("holdLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRunNotes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "noteFilter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "runNoteFilter"), com.orsyp.duws.RunNoteFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "runNote"));
        oper.setReturnClass(com.orsyp.duws.RunNote[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "runNotes"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"), com.orsyp.duws.EventId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "event"));
        oper.setReturnClass(com.orsyp.duws.Event.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "event"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListOutage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "outage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "outageFilter"), com.orsyp.duws.OutageFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "outageWindow"));
        oper.setReturnClass(com.orsyp.duws.OutageWindow[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "event"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "event"), com.orsyp.duws.Event.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getExecutionLogAsAttachment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        oper.setReturnClass(byte[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "dataHandler"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addLaunchFromTask2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), com.orsyp.duws.TaskId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "variables"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "variable"), com.orsyp.duws.Variable[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setReturnClass(com.orsyp.duws.LaunchId.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getLaunchFromTask");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"), com.orsyp.duws.TaskId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"));
        oper.setReturnClass(com.orsyp.duws.Launch.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("purgeExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getHistoryTrace");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "historyTrace"));
        oper.setReturnClass(com.orsyp.duws.HistoryTrace.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "historyTrace"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListNode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeFilter"), com.orsyp.duws.NodeFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeItem"));
        oper.setReturnClass(com.orsyp.duws.NodeItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("skipExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("rerunExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "startDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "endDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "batchQueue"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "step"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "byPassCheck"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("releaseLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getScriptResourceLog");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "resourceLog"));
        oper.setReturnClass(com.orsyp.duws.ResourceLog.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "resourceLog"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("stopExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"), com.orsyp.duws.ExecutionId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "delay"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getDUEnvironmentList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "token"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeFilter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsNodeFilter"), com.orsyp.duws.UvmsNodeFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "envir"));
        oper.setReturnClass(com.orsyp.duws.Envir[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "envList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[41] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("stopEngine");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "engine"), com.orsyp.duws.Engine.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[42] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "executionFilter"), com.orsyp.duws.ExecutionFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionItem"));
        oper.setReturnClass(com.orsyp.duws.ExecutionItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[43] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("bypassLaunchConditionCheck");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[44] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteOutageWindow");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "outage"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "outageWindow"), com.orsyp.duws.OutageWindow.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[45] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unacknowledgeExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[46] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("acknowledgeExecution");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[47] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("disableLaunch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"), com.orsyp.duws.LaunchId.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[48] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getWsVersion");
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "duwsVersion"));
        oper.setReturnClass(com.orsyp.duws.DuwsVersion.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "version"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        _operations[49] = oper;

    }

    private static void _initOperationDesc6(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSessionTree");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionItem"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionItem"), com.orsyp.duws.SessionItem.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionTree"));
        oper.setReturnClass(com.orsyp.duws.SessionTree.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionTree"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[50] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getListTask");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "taskFilter"), com.orsyp.duws.TaskFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskItem"));
        oper.setReturnClass(com.orsyp.duws.TaskItem[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[51] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("startQueue");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "context"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder"), com.orsyp.duws.ContextHolder.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"), com.orsyp.duws.Queue.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"),
                      "com.orsyp.duws.SessionTimedOutException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException"), 
                      true
                     ));
        _operations[52] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("login");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvms"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsContext"), com.orsyp.duws.UvmsContext.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://duws.orsyp.com", "token"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"),
                      "com.orsyp.duws.DuwsException",
                      new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException"), 
                      true
                     ));
        _operations[53] = oper;

    }

    public DuWebServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public DuWebServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public DuWebServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", ">execution>variables");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Variable[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "variable");
            qName2 = new javax.xml.namespace.QName("", "variables");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", ">launch>variables");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Variable[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "variable");
            qName2 = new javax.xml.namespace.QName("", "variables");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "context");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Context.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "contextHolder");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ContextHolder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "DuwsException");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.DuwsException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "duwsVersion");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.DuwsVersion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "engine");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Engine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "envir");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Envir.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "envirStatus");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.EnvirStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "event");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Event.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "eventFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.EventFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.EventId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "eventItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.EventItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "execution");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Execution.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "executionFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ExecutionFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ExecutionId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "executionItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ExecutionItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "executionLog");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ExecutionLog.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "historyTrace");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.HistoryTrace.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "launch");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Launch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "launchFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.LaunchFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.LaunchId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "launchItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.LaunchItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "muFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.MuFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "muId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.MuId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "muItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.MuItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.NodeFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.NodeId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.NodeItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "outageFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.OutageFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "outageWindow");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.OutageWindow.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "previousLaunch");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.PreviousLaunch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "queue");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Queue.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "resourceLog");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.ResourceLog.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "runBook");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.RunBook.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "runBookFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.RunBookFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "runNote");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.RunNote.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "runNoteFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.RunNoteFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionNode");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionNode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "SessionTimedOutException");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionTimedOutException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionTree");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.SessionTree.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "taskFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.TaskFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.TaskId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "taskItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.TaskItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.UprocFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocId");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.UprocId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocItem");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.UprocItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsContext");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.UvmsContext.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsNodeFilter");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.UvmsNodeFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://duws.orsyp.com", "variable");
            cachedSerQNames.add(qName);
            cls = com.orsyp.duws.Variable.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public void forceCompleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "forceCompleteLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.UprocItem[] getListUproc(com.orsyp.duws.ContextHolder context, com.orsyp.duws.UprocFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListUproc"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.UprocItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.UprocItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.UprocItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "deleteEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, eventId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.SessionItem[] getListSession(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.SessionItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.SessionItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.SessionItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.Launch getLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.Launch) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.Launch) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.Launch.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.LaunchId addLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "addLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launch});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.LaunchId) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.LaunchId) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.LaunchId.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void restartEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "restartEngine"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, engine});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.EventItem[] getListEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.EventItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.EventItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.EventItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.RunBook[] getRunBooks(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBookFilter runBookFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getRunBooks"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, runBookFilter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.RunBook[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.RunBook[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.RunBook[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.PreviousLaunch[] getPreviousLaunches(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getPreviousLaunches"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.PreviousLaunch[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.PreviousLaunch[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.PreviousLaunch[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.LaunchId addLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "addLaunchFromTask"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, taskId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.LaunchId) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.LaunchId) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.LaunchId.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void stopQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "stopQueue"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, queue});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void logout(java.lang.String token) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "logout"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {token});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void enableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "enableLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.MuItem[] getListMU(com.orsyp.duws.ContextHolder context, com.orsyp.duws.MuFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListMU"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.MuItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.MuItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.MuItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void startEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "startEngine"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, engine});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.ExecutionLog getExecutionLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getExecutionLog"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.ExecutionLog) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.ExecutionLog) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.ExecutionLog.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.Execution getExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.Execution) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.Execution) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.Execution.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void updateEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId, java.lang.String status, java.lang.String step) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, eventId, status, step});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void resetQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "resetQueue"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, queue});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void updateLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launch});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public byte[] getRunBookExternalFile(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBook runBook) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getRunBookExternalFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, runBook});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (byte[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (byte[]) org.apache.axis.utils.JavaUtils.convert(_resp, byte[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void createOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.holders.OutageWindowHolder outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "createOutageWindow"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, outage.value});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                outage.value = (com.orsyp.duws.OutageWindow) _output.get(new javax.xml.namespace.QName("http://duws.orsyp.com", "outage"));
            } catch (java.lang.Exception _exception) {
                outage.value = (com.orsyp.duws.OutageWindow) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("http://duws.orsyp.com", "outage")), com.orsyp.duws.OutageWindow.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.LaunchItem[] getListLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.LaunchItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.LaunchItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.LaunchItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void holdLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "holdLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launch});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.RunNote[] getRunNotes(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunNoteFilter noteFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getRunNotes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, noteFilter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.RunNote[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.RunNote[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.RunNote[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.Event getEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, eventId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.Event) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.Event) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.Event.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.OutageWindow[] getListOutage(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageFilter outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListOutage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, outage});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.OutageWindow[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.OutageWindow[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.OutageWindow[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void addEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Event event) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "addEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, event});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public byte[] getExecutionLogAsAttachment(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getExecutionLogAsAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (byte[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (byte[]) org.apache.axis.utils.JavaUtils.convert(_resp, byte[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.LaunchId addLaunchFromTask2(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId, com.orsyp.duws.Variable[] variables) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "addLaunchFromTask2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, taskId, variables});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.LaunchId) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.LaunchId) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.LaunchId.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.Launch getLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getLaunchFromTask"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, taskId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.Launch) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.Launch) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.Launch.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "deleteLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void purgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "purgeExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.HistoryTrace getHistoryTrace(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getHistoryTrace"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.HistoryTrace) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.HistoryTrace) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.HistoryTrace.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.NodeItem[] getListNode(com.orsyp.duws.ContextHolder context, com.orsyp.duws.NodeFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListNode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.NodeItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.NodeItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.NodeItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void skipExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "skipExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void rerunExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, java.lang.String startDate, java.lang.String endDate, java.lang.String batchQueue, java.lang.String user, int step, boolean byPassCheck) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "rerunExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId, startDate, endDate, batchQueue, user, new java.lang.Integer(step), new java.lang.Boolean(byPassCheck)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void releaseLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "releaseLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.ResourceLog getScriptResourceLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getScriptResourceLog"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.ResourceLog) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.ResourceLog) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.ResourceLog.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void stopExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, int delay) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "stopExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, executionId, new java.lang.Integer(delay)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.Envir[] getDUEnvironmentList(java.lang.String token, com.orsyp.duws.UvmsNodeFilter nodeFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getDUEnvironmentList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {token, nodeFilter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.Envir[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.Envir[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.Envir[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void stopEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[42]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "stopEngine"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, engine});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.ExecutionItem[] getListExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[43]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.ExecutionItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.ExecutionItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.ExecutionItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void bypassLaunchConditionCheck(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[44]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "bypassLaunchConditionCheck"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageWindow outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[45]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "deleteOutageWindow"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, outage});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void unacknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[46]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "unacknowledgeExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void acknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[47]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "acknowledgeExecution"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void disableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[48]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "disableLaunch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, launchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.DuwsVersion getWsVersion() throws java.rmi.RemoteException, com.orsyp.duws.DuwsException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[49]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getWsVersion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.DuwsVersion) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.DuwsVersion) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.DuwsVersion.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.SessionTree getSessionTree(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionItem sessionItem) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[50]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getSessionTree"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, sessionItem});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.SessionTree) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.SessionTree) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.SessionTree.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.orsyp.duws.TaskItem[] getListTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[51]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "getListTask"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.orsyp.duws.TaskItem[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.orsyp.duws.TaskItem[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.orsyp.duws.TaskItem[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void startQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[52]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "startQueue"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {context, queue});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.SessionTimedOutException) {
              throw (com.orsyp.duws.SessionTimedOutException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.String login(com.orsyp.duws.UvmsContext uvms) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[53]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://duws.orsyp.com", "login"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {uvms});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.orsyp.duws.DuwsException) {
              throw (com.orsyp.duws.DuwsException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}

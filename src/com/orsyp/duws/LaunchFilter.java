/**
 * LaunchFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class LaunchFilter  implements java.io.Serializable {
    private java.lang.String task;

    private java.lang.String session;

    private java.lang.String uproc;

    private java.lang.String mu;

    private java.lang.String status;

    private java.lang.String user;

    private java.lang.String beginDateMin;

    private java.lang.String beginDateMax;

    private java.lang.String beginHourMin;

    private java.lang.String beginHourMax;

    private java.lang.String endDateMin;

    private java.lang.String endDateMax;

    private java.lang.String endHourMin;

    private java.lang.String endHourMax;

    private java.lang.String processingDate;

    private java.lang.String numlancMin;

    private java.lang.String numlancMax;

    private java.lang.String numsessMin;

    private java.lang.String numsessMax;

    private java.lang.String numprocMin;

    private java.lang.String numprocMax;

    private java.lang.String orderByFields;

    private java.lang.String order;

    private java.lang.String maximumResults;

    public LaunchFilter() {
    }

    public LaunchFilter(
           java.lang.String task,
           java.lang.String session,
           java.lang.String uproc,
           java.lang.String mu,
           java.lang.String status,
           java.lang.String user,
           java.lang.String beginDateMin,
           java.lang.String beginDateMax,
           java.lang.String beginHourMin,
           java.lang.String beginHourMax,
           java.lang.String endDateMin,
           java.lang.String endDateMax,
           java.lang.String endHourMin,
           java.lang.String endHourMax,
           java.lang.String processingDate,
           java.lang.String numlancMin,
           java.lang.String numlancMax,
           java.lang.String numsessMin,
           java.lang.String numsessMax,
           java.lang.String numprocMin,
           java.lang.String numprocMax,
           java.lang.String orderByFields,
           java.lang.String order,
           java.lang.String maximumResults) {
           this.task = task;
           this.session = session;
           this.uproc = uproc;
           this.mu = mu;
           this.status = status;
           this.user = user;
           this.beginDateMin = beginDateMin;
           this.beginDateMax = beginDateMax;
           this.beginHourMin = beginHourMin;
           this.beginHourMax = beginHourMax;
           this.endDateMin = endDateMin;
           this.endDateMax = endDateMax;
           this.endHourMin = endHourMin;
           this.endHourMax = endHourMax;
           this.processingDate = processingDate;
           this.numlancMin = numlancMin;
           this.numlancMax = numlancMax;
           this.numsessMin = numsessMin;
           this.numsessMax = numsessMax;
           this.numprocMin = numprocMin;
           this.numprocMax = numprocMax;
           this.orderByFields = orderByFields;
           this.order = order;
           this.maximumResults = maximumResults;
    }


    /**
     * Gets the task value for this LaunchFilter.
     * 
     * @return task
     */
    public java.lang.String getTask() {
        return task;
    }


    /**
     * Sets the task value for this LaunchFilter.
     * 
     * @param task
     */
    public void setTask(java.lang.String task) {
        this.task = task;
    }


    /**
     * Gets the session value for this LaunchFilter.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this LaunchFilter.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the uproc value for this LaunchFilter.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this LaunchFilter.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the mu value for this LaunchFilter.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this LaunchFilter.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the status value for this LaunchFilter.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this LaunchFilter.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the user value for this LaunchFilter.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this LaunchFilter.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }


    /**
     * Gets the beginDateMin value for this LaunchFilter.
     * 
     * @return beginDateMin
     */
    public java.lang.String getBeginDateMin() {
        return beginDateMin;
    }


    /**
     * Sets the beginDateMin value for this LaunchFilter.
     * 
     * @param beginDateMin
     */
    public void setBeginDateMin(java.lang.String beginDateMin) {
        this.beginDateMin = beginDateMin;
    }


    /**
     * Gets the beginDateMax value for this LaunchFilter.
     * 
     * @return beginDateMax
     */
    public java.lang.String getBeginDateMax() {
        return beginDateMax;
    }


    /**
     * Sets the beginDateMax value for this LaunchFilter.
     * 
     * @param beginDateMax
     */
    public void setBeginDateMax(java.lang.String beginDateMax) {
        this.beginDateMax = beginDateMax;
    }


    /**
     * Gets the beginHourMin value for this LaunchFilter.
     * 
     * @return beginHourMin
     */
    public java.lang.String getBeginHourMin() {
        return beginHourMin;
    }


    /**
     * Sets the beginHourMin value for this LaunchFilter.
     * 
     * @param beginHourMin
     */
    public void setBeginHourMin(java.lang.String beginHourMin) {
        this.beginHourMin = beginHourMin;
    }


    /**
     * Gets the beginHourMax value for this LaunchFilter.
     * 
     * @return beginHourMax
     */
    public java.lang.String getBeginHourMax() {
        return beginHourMax;
    }


    /**
     * Sets the beginHourMax value for this LaunchFilter.
     * 
     * @param beginHourMax
     */
    public void setBeginHourMax(java.lang.String beginHourMax) {
        this.beginHourMax = beginHourMax;
    }


    /**
     * Gets the endDateMin value for this LaunchFilter.
     * 
     * @return endDateMin
     */
    public java.lang.String getEndDateMin() {
        return endDateMin;
    }


    /**
     * Sets the endDateMin value for this LaunchFilter.
     * 
     * @param endDateMin
     */
    public void setEndDateMin(java.lang.String endDateMin) {
        this.endDateMin = endDateMin;
    }


    /**
     * Gets the endDateMax value for this LaunchFilter.
     * 
     * @return endDateMax
     */
    public java.lang.String getEndDateMax() {
        return endDateMax;
    }


    /**
     * Sets the endDateMax value for this LaunchFilter.
     * 
     * @param endDateMax
     */
    public void setEndDateMax(java.lang.String endDateMax) {
        this.endDateMax = endDateMax;
    }


    /**
     * Gets the endHourMin value for this LaunchFilter.
     * 
     * @return endHourMin
     */
    public java.lang.String getEndHourMin() {
        return endHourMin;
    }


    /**
     * Sets the endHourMin value for this LaunchFilter.
     * 
     * @param endHourMin
     */
    public void setEndHourMin(java.lang.String endHourMin) {
        this.endHourMin = endHourMin;
    }


    /**
     * Gets the endHourMax value for this LaunchFilter.
     * 
     * @return endHourMax
     */
    public java.lang.String getEndHourMax() {
        return endHourMax;
    }


    /**
     * Sets the endHourMax value for this LaunchFilter.
     * 
     * @param endHourMax
     */
    public void setEndHourMax(java.lang.String endHourMax) {
        this.endHourMax = endHourMax;
    }


    /**
     * Gets the processingDate value for this LaunchFilter.
     * 
     * @return processingDate
     */
    public java.lang.String getProcessingDate() {
        return processingDate;
    }


    /**
     * Sets the processingDate value for this LaunchFilter.
     * 
     * @param processingDate
     */
    public void setProcessingDate(java.lang.String processingDate) {
        this.processingDate = processingDate;
    }


    /**
     * Gets the numlancMin value for this LaunchFilter.
     * 
     * @return numlancMin
     */
    public java.lang.String getNumlancMin() {
        return numlancMin;
    }


    /**
     * Sets the numlancMin value for this LaunchFilter.
     * 
     * @param numlancMin
     */
    public void setNumlancMin(java.lang.String numlancMin) {
        this.numlancMin = numlancMin;
    }


    /**
     * Gets the numlancMax value for this LaunchFilter.
     * 
     * @return numlancMax
     */
    public java.lang.String getNumlancMax() {
        return numlancMax;
    }


    /**
     * Sets the numlancMax value for this LaunchFilter.
     * 
     * @param numlancMax
     */
    public void setNumlancMax(java.lang.String numlancMax) {
        this.numlancMax = numlancMax;
    }


    /**
     * Gets the numsessMin value for this LaunchFilter.
     * 
     * @return numsessMin
     */
    public java.lang.String getNumsessMin() {
        return numsessMin;
    }


    /**
     * Sets the numsessMin value for this LaunchFilter.
     * 
     * @param numsessMin
     */
    public void setNumsessMin(java.lang.String numsessMin) {
        this.numsessMin = numsessMin;
    }


    /**
     * Gets the numsessMax value for this LaunchFilter.
     * 
     * @return numsessMax
     */
    public java.lang.String getNumsessMax() {
        return numsessMax;
    }


    /**
     * Sets the numsessMax value for this LaunchFilter.
     * 
     * @param numsessMax
     */
    public void setNumsessMax(java.lang.String numsessMax) {
        this.numsessMax = numsessMax;
    }


    /**
     * Gets the numprocMin value for this LaunchFilter.
     * 
     * @return numprocMin
     */
    public java.lang.String getNumprocMin() {
        return numprocMin;
    }


    /**
     * Sets the numprocMin value for this LaunchFilter.
     * 
     * @param numprocMin
     */
    public void setNumprocMin(java.lang.String numprocMin) {
        this.numprocMin = numprocMin;
    }


    /**
     * Gets the numprocMax value for this LaunchFilter.
     * 
     * @return numprocMax
     */
    public java.lang.String getNumprocMax() {
        return numprocMax;
    }


    /**
     * Sets the numprocMax value for this LaunchFilter.
     * 
     * @param numprocMax
     */
    public void setNumprocMax(java.lang.String numprocMax) {
        this.numprocMax = numprocMax;
    }


    /**
     * Gets the orderByFields value for this LaunchFilter.
     * 
     * @return orderByFields
     */
    public java.lang.String getOrderByFields() {
        return orderByFields;
    }


    /**
     * Sets the orderByFields value for this LaunchFilter.
     * 
     * @param orderByFields
     */
    public void setOrderByFields(java.lang.String orderByFields) {
        this.orderByFields = orderByFields;
    }


    /**
     * Gets the order value for this LaunchFilter.
     * 
     * @return order
     */
    public java.lang.String getOrder() {
        return order;
    }


    /**
     * Sets the order value for this LaunchFilter.
     * 
     * @param order
     */
    public void setOrder(java.lang.String order) {
        this.order = order;
    }


    /**
     * Gets the maximumResults value for this LaunchFilter.
     * 
     * @return maximumResults
     */
    public java.lang.String getMaximumResults() {
        return maximumResults;
    }


    /**
     * Sets the maximumResults value for this LaunchFilter.
     * 
     * @param maximumResults
     */
    public void setMaximumResults(java.lang.String maximumResults) {
        this.maximumResults = maximumResults;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LaunchFilter)) return false;
        LaunchFilter other = (LaunchFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              this.task.equals(other.getTask()))) &&
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.beginDateMin==null && other.getBeginDateMin()==null) || 
             (this.beginDateMin!=null &&
              this.beginDateMin.equals(other.getBeginDateMin()))) &&
            ((this.beginDateMax==null && other.getBeginDateMax()==null) || 
             (this.beginDateMax!=null &&
              this.beginDateMax.equals(other.getBeginDateMax()))) &&
            ((this.beginHourMin==null && other.getBeginHourMin()==null) || 
             (this.beginHourMin!=null &&
              this.beginHourMin.equals(other.getBeginHourMin()))) &&
            ((this.beginHourMax==null && other.getBeginHourMax()==null) || 
             (this.beginHourMax!=null &&
              this.beginHourMax.equals(other.getBeginHourMax()))) &&
            ((this.endDateMin==null && other.getEndDateMin()==null) || 
             (this.endDateMin!=null &&
              this.endDateMin.equals(other.getEndDateMin()))) &&
            ((this.endDateMax==null && other.getEndDateMax()==null) || 
             (this.endDateMax!=null &&
              this.endDateMax.equals(other.getEndDateMax()))) &&
            ((this.endHourMin==null && other.getEndHourMin()==null) || 
             (this.endHourMin!=null &&
              this.endHourMin.equals(other.getEndHourMin()))) &&
            ((this.endHourMax==null && other.getEndHourMax()==null) || 
             (this.endHourMax!=null &&
              this.endHourMax.equals(other.getEndHourMax()))) &&
            ((this.processingDate==null && other.getProcessingDate()==null) || 
             (this.processingDate!=null &&
              this.processingDate.equals(other.getProcessingDate()))) &&
            ((this.numlancMin==null && other.getNumlancMin()==null) || 
             (this.numlancMin!=null &&
              this.numlancMin.equals(other.getNumlancMin()))) &&
            ((this.numlancMax==null && other.getNumlancMax()==null) || 
             (this.numlancMax!=null &&
              this.numlancMax.equals(other.getNumlancMax()))) &&
            ((this.numsessMin==null && other.getNumsessMin()==null) || 
             (this.numsessMin!=null &&
              this.numsessMin.equals(other.getNumsessMin()))) &&
            ((this.numsessMax==null && other.getNumsessMax()==null) || 
             (this.numsessMax!=null &&
              this.numsessMax.equals(other.getNumsessMax()))) &&
            ((this.numprocMin==null && other.getNumprocMin()==null) || 
             (this.numprocMin!=null &&
              this.numprocMin.equals(other.getNumprocMin()))) &&
            ((this.numprocMax==null && other.getNumprocMax()==null) || 
             (this.numprocMax!=null &&
              this.numprocMax.equals(other.getNumprocMax()))) &&
            ((this.orderByFields==null && other.getOrderByFields()==null) || 
             (this.orderByFields!=null &&
              this.orderByFields.equals(other.getOrderByFields()))) &&
            ((this.order==null && other.getOrder()==null) || 
             (this.order!=null &&
              this.order.equals(other.getOrder()))) &&
            ((this.maximumResults==null && other.getMaximumResults()==null) || 
             (this.maximumResults!=null &&
              this.maximumResults.equals(other.getMaximumResults())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTask() != null) {
            _hashCode += getTask().hashCode();
        }
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getBeginDateMin() != null) {
            _hashCode += getBeginDateMin().hashCode();
        }
        if (getBeginDateMax() != null) {
            _hashCode += getBeginDateMax().hashCode();
        }
        if (getBeginHourMin() != null) {
            _hashCode += getBeginHourMin().hashCode();
        }
        if (getBeginHourMax() != null) {
            _hashCode += getBeginHourMax().hashCode();
        }
        if (getEndDateMin() != null) {
            _hashCode += getEndDateMin().hashCode();
        }
        if (getEndDateMax() != null) {
            _hashCode += getEndDateMax().hashCode();
        }
        if (getEndHourMin() != null) {
            _hashCode += getEndHourMin().hashCode();
        }
        if (getEndHourMax() != null) {
            _hashCode += getEndHourMax().hashCode();
        }
        if (getProcessingDate() != null) {
            _hashCode += getProcessingDate().hashCode();
        }
        if (getNumlancMin() != null) {
            _hashCode += getNumlancMin().hashCode();
        }
        if (getNumlancMax() != null) {
            _hashCode += getNumlancMax().hashCode();
        }
        if (getNumsessMin() != null) {
            _hashCode += getNumsessMin().hashCode();
        }
        if (getNumsessMax() != null) {
            _hashCode += getNumsessMax().hashCode();
        }
        if (getNumprocMin() != null) {
            _hashCode += getNumprocMin().hashCode();
        }
        if (getNumprocMax() != null) {
            _hashCode += getNumprocMax().hashCode();
        }
        if (getOrderByFields() != null) {
            _hashCode += getOrderByFields().hashCode();
        }
        if (getOrder() != null) {
            _hashCode += getOrder().hashCode();
        }
        if (getMaximumResults() != null) {
            _hashCode += getMaximumResults().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LaunchFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDateMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginDateMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDateMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginDateMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginHourMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginHourMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginHourMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginHourMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDateMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endDateMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDateMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endDateMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endHourMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endHourMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endHourMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endHourMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "processingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numlancMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numlancMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numlancMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numlancMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numsessMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numsessMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numsessMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numsessMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numprocMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numprocMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numprocMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numprocMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderByFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "orderByFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumResults");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "maximumResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

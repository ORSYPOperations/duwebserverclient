/**
 * UprocId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class UprocId  implements java.io.Serializable {
    private java.lang.String uproc;

    private java.lang.String uprocVersion;

    public UprocId() {
    }

    public UprocId(
           java.lang.String uproc,
           java.lang.String uprocVersion) {
           this.uproc = uproc;
           this.uprocVersion = uprocVersion;
    }


    /**
     * Gets the uproc value for this UprocId.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this UprocId.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the uprocVersion value for this UprocId.
     * 
     * @return uprocVersion
     */
    public java.lang.String getUprocVersion() {
        return uprocVersion;
    }


    /**
     * Sets the uprocVersion value for this UprocId.
     * 
     * @param uprocVersion
     */
    public void setUprocVersion(java.lang.String uprocVersion) {
        this.uprocVersion = uprocVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UprocId)) return false;
        UprocId other = (UprocId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.uprocVersion==null && other.getUprocVersion()==null) || 
             (this.uprocVersion!=null &&
              this.uprocVersion.equals(other.getUprocVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getUprocVersion() != null) {
            _hashCode += getUprocVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UprocId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

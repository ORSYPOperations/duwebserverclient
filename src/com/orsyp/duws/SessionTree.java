/**
 * SessionTree.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class SessionTree  implements java.io.Serializable {
    private java.lang.String sessionName;

    private java.lang.String sessionVersion;

    private com.orsyp.duws.SessionNode root;

    public SessionTree() {
    }

    public SessionTree(
           java.lang.String sessionName,
           java.lang.String sessionVersion,
           com.orsyp.duws.SessionNode root) {
           this.sessionName = sessionName;
           this.sessionVersion = sessionVersion;
           this.root = root;
    }


    /**
     * Gets the sessionName value for this SessionTree.
     * 
     * @return sessionName
     */
    public java.lang.String getSessionName() {
        return sessionName;
    }


    /**
     * Sets the sessionName value for this SessionTree.
     * 
     * @param sessionName
     */
    public void setSessionName(java.lang.String sessionName) {
        this.sessionName = sessionName;
    }


    /**
     * Gets the sessionVersion value for this SessionTree.
     * 
     * @return sessionVersion
     */
    public java.lang.String getSessionVersion() {
        return sessionVersion;
    }


    /**
     * Sets the sessionVersion value for this SessionTree.
     * 
     * @param sessionVersion
     */
    public void setSessionVersion(java.lang.String sessionVersion) {
        this.sessionVersion = sessionVersion;
    }


    /**
     * Gets the root value for this SessionTree.
     * 
     * @return root
     */
    public com.orsyp.duws.SessionNode getRoot() {
        return root;
    }


    /**
     * Sets the root value for this SessionTree.
     * 
     * @param root
     */
    public void setRoot(com.orsyp.duws.SessionNode root) {
        this.root = root;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SessionTree)) return false;
        SessionTree other = (SessionTree) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionName==null && other.getSessionName()==null) || 
             (this.sessionName!=null &&
              this.sessionName.equals(other.getSessionName()))) &&
            ((this.sessionVersion==null && other.getSessionVersion()==null) || 
             (this.sessionVersion!=null &&
              this.sessionVersion.equals(other.getSessionVersion()))) &&
            ((this.root==null && other.getRoot()==null) || 
             (this.root!=null &&
              this.root.equals(other.getRoot())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionName() != null) {
            _hashCode += getSessionName().hashCode();
        }
        if (getSessionVersion() != null) {
            _hashCode += getSessionVersion().hashCode();
        }
        if (getRoot() != null) {
            _hashCode += getRoot().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SessionTree.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionTree"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("root");
        elemField.setXmlName(new javax.xml.namespace.QName("", "root"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionNode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

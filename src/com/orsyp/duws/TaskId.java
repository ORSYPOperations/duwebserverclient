/**
 * TaskId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class TaskId  implements java.io.Serializable {
    private java.lang.String task;

    private java.lang.String session;

    private java.lang.String sessionVersion;

    private java.lang.String uproc;

    private java.lang.String uprocVersion;

    private java.lang.String mu;

    private boolean template;

    public TaskId() {
    }

    public TaskId(
           java.lang.String task,
           java.lang.String session,
           java.lang.String sessionVersion,
           java.lang.String uproc,
           java.lang.String uprocVersion,
           java.lang.String mu,
           boolean template) {
           this.task = task;
           this.session = session;
           this.sessionVersion = sessionVersion;
           this.uproc = uproc;
           this.uprocVersion = uprocVersion;
           this.mu = mu;
           this.template = template;
    }


    /**
     * Gets the task value for this TaskId.
     * 
     * @return task
     */
    public java.lang.String getTask() {
        return task;
    }


    /**
     * Sets the task value for this TaskId.
     * 
     * @param task
     */
    public void setTask(java.lang.String task) {
        this.task = task;
    }


    /**
     * Gets the session value for this TaskId.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this TaskId.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the sessionVersion value for this TaskId.
     * 
     * @return sessionVersion
     */
    public java.lang.String getSessionVersion() {
        return sessionVersion;
    }


    /**
     * Sets the sessionVersion value for this TaskId.
     * 
     * @param sessionVersion
     */
    public void setSessionVersion(java.lang.String sessionVersion) {
        this.sessionVersion = sessionVersion;
    }


    /**
     * Gets the uproc value for this TaskId.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this TaskId.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the uprocVersion value for this TaskId.
     * 
     * @return uprocVersion
     */
    public java.lang.String getUprocVersion() {
        return uprocVersion;
    }


    /**
     * Sets the uprocVersion value for this TaskId.
     * 
     * @param uprocVersion
     */
    public void setUprocVersion(java.lang.String uprocVersion) {
        this.uprocVersion = uprocVersion;
    }


    /**
     * Gets the mu value for this TaskId.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this TaskId.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the template value for this TaskId.
     * 
     * @return template
     */
    public boolean isTemplate() {
        return template;
    }


    /**
     * Sets the template value for this TaskId.
     * 
     * @param template
     */
    public void setTemplate(boolean template) {
        this.template = template;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaskId)) return false;
        TaskId other = (TaskId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              this.task.equals(other.getTask()))) &&
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.sessionVersion==null && other.getSessionVersion()==null) || 
             (this.sessionVersion!=null &&
              this.sessionVersion.equals(other.getSessionVersion()))) &&
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.uprocVersion==null && other.getUprocVersion()==null) || 
             (this.uprocVersion!=null &&
              this.uprocVersion.equals(other.getUprocVersion()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            this.template == other.isTemplate();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTask() != null) {
            _hashCode += getTask().hashCode();
        }
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getSessionVersion() != null) {
            _hashCode += getSessionVersion().hashCode();
        }
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getUprocVersion() != null) {
            _hashCode += getUprocVersion().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        _hashCode += (isTemplate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("template");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "template"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * EnvirStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class EnvirStatus implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnvirStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CONNECTING = "CONNECTING";
    public static final java.lang.String _CONNECTED = "CONNECTED";
    public static final java.lang.String _FAILED = "FAILED";
    public static final java.lang.String _BROKEN = "BROKEN";
    public static final java.lang.String _STOPPED = "STOPPED";
    public static final java.lang.String _UNREACHABLE = "UNREACHABLE";
    public static final java.lang.String _NODE_UNREACHABLE = "NODE_UNREACHABLE";
    public static final java.lang.String _IO_UNREACHABLE = "IO_UNREACHABLE";
    public static final java.lang.String _CONNECTED_WARNING = "CONNECTED_WARNING";
    public static final java.lang.String _UNKNOWN = "UNKNOWN";
    public static final java.lang.String _NO_UPWARDS_MESSAGE = "NO_UPWARDS_MESSAGE";
    public static final java.lang.String _BEING_COMPUTED = "BEING_COMPUTED";
    public static final EnvirStatus CONNECTING = new EnvirStatus(_CONNECTING);
    public static final EnvirStatus CONNECTED = new EnvirStatus(_CONNECTED);
    public static final EnvirStatus FAILED = new EnvirStatus(_FAILED);
    public static final EnvirStatus BROKEN = new EnvirStatus(_BROKEN);
    public static final EnvirStatus STOPPED = new EnvirStatus(_STOPPED);
    public static final EnvirStatus UNREACHABLE = new EnvirStatus(_UNREACHABLE);
    public static final EnvirStatus NODE_UNREACHABLE = new EnvirStatus(_NODE_UNREACHABLE);
    public static final EnvirStatus IO_UNREACHABLE = new EnvirStatus(_IO_UNREACHABLE);
    public static final EnvirStatus CONNECTED_WARNING = new EnvirStatus(_CONNECTED_WARNING);
    public static final EnvirStatus UNKNOWN = new EnvirStatus(_UNKNOWN);
    public static final EnvirStatus NO_UPWARDS_MESSAGE = new EnvirStatus(_NO_UPWARDS_MESSAGE);
    public static final EnvirStatus BEING_COMPUTED = new EnvirStatus(_BEING_COMPUTED);
    public java.lang.String getValue() { return _value_;}
    public static EnvirStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnvirStatus enumeration = (EnvirStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnvirStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnvirStatus.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "envirStatus"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}

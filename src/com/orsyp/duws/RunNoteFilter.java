/**
 * RunNoteFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class RunNoteFilter  implements java.io.Serializable {
    private java.lang.String session;

    private java.lang.String sessionVersion;

    private java.lang.String uproc;

    private java.lang.String uprocVersion;

    private java.lang.String task;

    private java.lang.String taskVersion;

    private java.lang.String mu;

    private java.lang.String numnote;

    private java.lang.String numlanc;

    private java.lang.String numsess;

    public RunNoteFilter() {
    }

    public RunNoteFilter(
           java.lang.String session,
           java.lang.String sessionVersion,
           java.lang.String uproc,
           java.lang.String uprocVersion,
           java.lang.String task,
           java.lang.String taskVersion,
           java.lang.String mu,
           java.lang.String numnote,
           java.lang.String numlanc,
           java.lang.String numsess) {
           this.session = session;
           this.sessionVersion = sessionVersion;
           this.uproc = uproc;
           this.uprocVersion = uprocVersion;
           this.task = task;
           this.taskVersion = taskVersion;
           this.mu = mu;
           this.numnote = numnote;
           this.numlanc = numlanc;
           this.numsess = numsess;
    }


    /**
     * Gets the session value for this RunNoteFilter.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this RunNoteFilter.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the sessionVersion value for this RunNoteFilter.
     * 
     * @return sessionVersion
     */
    public java.lang.String getSessionVersion() {
        return sessionVersion;
    }


    /**
     * Sets the sessionVersion value for this RunNoteFilter.
     * 
     * @param sessionVersion
     */
    public void setSessionVersion(java.lang.String sessionVersion) {
        this.sessionVersion = sessionVersion;
    }


    /**
     * Gets the uproc value for this RunNoteFilter.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this RunNoteFilter.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the uprocVersion value for this RunNoteFilter.
     * 
     * @return uprocVersion
     */
    public java.lang.String getUprocVersion() {
        return uprocVersion;
    }


    /**
     * Sets the uprocVersion value for this RunNoteFilter.
     * 
     * @param uprocVersion
     */
    public void setUprocVersion(java.lang.String uprocVersion) {
        this.uprocVersion = uprocVersion;
    }


    /**
     * Gets the task value for this RunNoteFilter.
     * 
     * @return task
     */
    public java.lang.String getTask() {
        return task;
    }


    /**
     * Sets the task value for this RunNoteFilter.
     * 
     * @param task
     */
    public void setTask(java.lang.String task) {
        this.task = task;
    }


    /**
     * Gets the taskVersion value for this RunNoteFilter.
     * 
     * @return taskVersion
     */
    public java.lang.String getTaskVersion() {
        return taskVersion;
    }


    /**
     * Sets the taskVersion value for this RunNoteFilter.
     * 
     * @param taskVersion
     */
    public void setTaskVersion(java.lang.String taskVersion) {
        this.taskVersion = taskVersion;
    }


    /**
     * Gets the mu value for this RunNoteFilter.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this RunNoteFilter.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the numnote value for this RunNoteFilter.
     * 
     * @return numnote
     */
    public java.lang.String getNumnote() {
        return numnote;
    }


    /**
     * Sets the numnote value for this RunNoteFilter.
     * 
     * @param numnote
     */
    public void setNumnote(java.lang.String numnote) {
        this.numnote = numnote;
    }


    /**
     * Gets the numlanc value for this RunNoteFilter.
     * 
     * @return numlanc
     */
    public java.lang.String getNumlanc() {
        return numlanc;
    }


    /**
     * Sets the numlanc value for this RunNoteFilter.
     * 
     * @param numlanc
     */
    public void setNumlanc(java.lang.String numlanc) {
        this.numlanc = numlanc;
    }


    /**
     * Gets the numsess value for this RunNoteFilter.
     * 
     * @return numsess
     */
    public java.lang.String getNumsess() {
        return numsess;
    }


    /**
     * Sets the numsess value for this RunNoteFilter.
     * 
     * @param numsess
     */
    public void setNumsess(java.lang.String numsess) {
        this.numsess = numsess;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunNoteFilter)) return false;
        RunNoteFilter other = (RunNoteFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.sessionVersion==null && other.getSessionVersion()==null) || 
             (this.sessionVersion!=null &&
              this.sessionVersion.equals(other.getSessionVersion()))) &&
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.uprocVersion==null && other.getUprocVersion()==null) || 
             (this.uprocVersion!=null &&
              this.uprocVersion.equals(other.getUprocVersion()))) &&
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              this.task.equals(other.getTask()))) &&
            ((this.taskVersion==null && other.getTaskVersion()==null) || 
             (this.taskVersion!=null &&
              this.taskVersion.equals(other.getTaskVersion()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            ((this.numnote==null && other.getNumnote()==null) || 
             (this.numnote!=null &&
              this.numnote.equals(other.getNumnote()))) &&
            ((this.numlanc==null && other.getNumlanc()==null) || 
             (this.numlanc!=null &&
              this.numlanc.equals(other.getNumlanc()))) &&
            ((this.numsess==null && other.getNumsess()==null) || 
             (this.numsess!=null &&
              this.numsess.equals(other.getNumsess())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getSessionVersion() != null) {
            _hashCode += getSessionVersion().hashCode();
        }
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getUprocVersion() != null) {
            _hashCode += getUprocVersion().hashCode();
        }
        if (getTask() != null) {
            _hashCode += getTask().hashCode();
        }
        if (getTaskVersion() != null) {
            _hashCode += getTaskVersion().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        if (getNumnote() != null) {
            _hashCode += getNumnote().hashCode();
        }
        if (getNumlanc() != null) {
            _hashCode += getNumlanc().hashCode();
        }
        if (getNumsess() != null) {
            _hashCode += getNumsess().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunNoteFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "runNoteFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "taskVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numnote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numnote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numlanc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numlanc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numsess");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numsess"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * LaunchId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class LaunchId  implements java.io.Serializable {
    private java.lang.String task;

    private java.lang.String session;

    private java.lang.String sessionVersion;

    private java.lang.String uproc;

    private java.lang.String uprocVersion;

    private java.lang.String mu;

    private java.lang.String numLanc;

    private java.lang.String numSess;

    private java.lang.String numProc;

    public LaunchId() {
    }

    public LaunchId(
           java.lang.String task,
           java.lang.String session,
           java.lang.String sessionVersion,
           java.lang.String uproc,
           java.lang.String uprocVersion,
           java.lang.String mu,
           java.lang.String numLanc,
           java.lang.String numSess,
           java.lang.String numProc) {
           this.task = task;
           this.session = session;
           this.sessionVersion = sessionVersion;
           this.uproc = uproc;
           this.uprocVersion = uprocVersion;
           this.mu = mu;
           this.numLanc = numLanc;
           this.numSess = numSess;
           this.numProc = numProc;
    }


    /**
     * Gets the task value for this LaunchId.
     * 
     * @return task
     */
    public java.lang.String getTask() {
        return task;
    }


    /**
     * Sets the task value for this LaunchId.
     * 
     * @param task
     */
    public void setTask(java.lang.String task) {
        this.task = task;
    }


    /**
     * Gets the session value for this LaunchId.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this LaunchId.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the sessionVersion value for this LaunchId.
     * 
     * @return sessionVersion
     */
    public java.lang.String getSessionVersion() {
        return sessionVersion;
    }


    /**
     * Sets the sessionVersion value for this LaunchId.
     * 
     * @param sessionVersion
     */
    public void setSessionVersion(java.lang.String sessionVersion) {
        this.sessionVersion = sessionVersion;
    }


    /**
     * Gets the uproc value for this LaunchId.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this LaunchId.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the uprocVersion value for this LaunchId.
     * 
     * @return uprocVersion
     */
    public java.lang.String getUprocVersion() {
        return uprocVersion;
    }


    /**
     * Sets the uprocVersion value for this LaunchId.
     * 
     * @param uprocVersion
     */
    public void setUprocVersion(java.lang.String uprocVersion) {
        this.uprocVersion = uprocVersion;
    }


    /**
     * Gets the mu value for this LaunchId.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this LaunchId.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the numLanc value for this LaunchId.
     * 
     * @return numLanc
     */
    public java.lang.String getNumLanc() {
        return numLanc;
    }


    /**
     * Sets the numLanc value for this LaunchId.
     * 
     * @param numLanc
     */
    public void setNumLanc(java.lang.String numLanc) {
        this.numLanc = numLanc;
    }


    /**
     * Gets the numSess value for this LaunchId.
     * 
     * @return numSess
     */
    public java.lang.String getNumSess() {
        return numSess;
    }


    /**
     * Sets the numSess value for this LaunchId.
     * 
     * @param numSess
     */
    public void setNumSess(java.lang.String numSess) {
        this.numSess = numSess;
    }


    /**
     * Gets the numProc value for this LaunchId.
     * 
     * @return numProc
     */
    public java.lang.String getNumProc() {
        return numProc;
    }


    /**
     * Sets the numProc value for this LaunchId.
     * 
     * @param numProc
     */
    public void setNumProc(java.lang.String numProc) {
        this.numProc = numProc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LaunchId)) return false;
        LaunchId other = (LaunchId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              this.task.equals(other.getTask()))) &&
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.sessionVersion==null && other.getSessionVersion()==null) || 
             (this.sessionVersion!=null &&
              this.sessionVersion.equals(other.getSessionVersion()))) &&
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.uprocVersion==null && other.getUprocVersion()==null) || 
             (this.uprocVersion!=null &&
              this.uprocVersion.equals(other.getUprocVersion()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            ((this.numLanc==null && other.getNumLanc()==null) || 
             (this.numLanc!=null &&
              this.numLanc.equals(other.getNumLanc()))) &&
            ((this.numSess==null && other.getNumSess()==null) || 
             (this.numSess!=null &&
              this.numSess.equals(other.getNumSess()))) &&
            ((this.numProc==null && other.getNumProc()==null) || 
             (this.numProc!=null &&
              this.numProc.equals(other.getNumProc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTask() != null) {
            _hashCode += getTask().hashCode();
        }
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getSessionVersion() != null) {
            _hashCode += getSessionVersion().hashCode();
        }
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getUprocVersion() != null) {
            _hashCode += getUprocVersion().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        if (getNumLanc() != null) {
            _hashCode += getNumLanc().hashCode();
        }
        if (getNumSess() != null) {
            _hashCode += getNumSess().hashCode();
        }
        if (getNumProc() != null) {
            _hashCode += getNumProc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LaunchId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLanc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numLanc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numSess");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numSess"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numProc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numProc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

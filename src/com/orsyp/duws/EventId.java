/**
 * EventId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class EventId  implements java.io.Serializable {
    private java.lang.String uproc;

    private java.lang.String session;

    private java.lang.String mu;

    private java.lang.String processingDate;

    private java.lang.String numProc;

    private java.lang.String numSess;

    private java.lang.String user;

    public EventId() {
    }

    public EventId(
           java.lang.String uproc,
           java.lang.String session,
           java.lang.String mu,
           java.lang.String processingDate,
           java.lang.String numProc,
           java.lang.String numSess,
           java.lang.String user) {
           this.uproc = uproc;
           this.session = session;
           this.mu = mu;
           this.processingDate = processingDate;
           this.numProc = numProc;
           this.numSess = numSess;
           this.user = user;
    }


    /**
     * Gets the uproc value for this EventId.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this EventId.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the session value for this EventId.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this EventId.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the mu value for this EventId.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this EventId.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the processingDate value for this EventId.
     * 
     * @return processingDate
     */
    public java.lang.String getProcessingDate() {
        return processingDate;
    }


    /**
     * Sets the processingDate value for this EventId.
     * 
     * @param processingDate
     */
    public void setProcessingDate(java.lang.String processingDate) {
        this.processingDate = processingDate;
    }


    /**
     * Gets the numProc value for this EventId.
     * 
     * @return numProc
     */
    public java.lang.String getNumProc() {
        return numProc;
    }


    /**
     * Sets the numProc value for this EventId.
     * 
     * @param numProc
     */
    public void setNumProc(java.lang.String numProc) {
        this.numProc = numProc;
    }


    /**
     * Gets the numSess value for this EventId.
     * 
     * @return numSess
     */
    public java.lang.String getNumSess() {
        return numSess;
    }


    /**
     * Sets the numSess value for this EventId.
     * 
     * @param numSess
     */
    public void setNumSess(java.lang.String numSess) {
        this.numSess = numSess;
    }


    /**
     * Gets the user value for this EventId.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this EventId.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventId)) return false;
        EventId other = (EventId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            ((this.processingDate==null && other.getProcessingDate()==null) || 
             (this.processingDate!=null &&
              this.processingDate.equals(other.getProcessingDate()))) &&
            ((this.numProc==null && other.getNumProc()==null) || 
             (this.numProc!=null &&
              this.numProc.equals(other.getNumProc()))) &&
            ((this.numSess==null && other.getNumSess()==null) || 
             (this.numSess!=null &&
              this.numSess.equals(other.getNumSess()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        if (getProcessingDate() != null) {
            _hashCode += getProcessingDate().hashCode();
        }
        if (getNumProc() != null) {
            _hashCode += getNumProc().hashCode();
        }
        if (getNumSess() != null) {
            _hashCode += getNumSess().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "processingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numProc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numProc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numSess");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numSess"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

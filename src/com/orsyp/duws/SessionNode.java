/**
 * SessionNode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class SessionNode  implements java.io.Serializable {
    private com.orsyp.duws.SessionNode[] childKO;

    private com.orsyp.duws.SessionNode[] childOK;

    private boolean header;

    private boolean leaf;

    private int rankInSession;

    private java.lang.String uprocName;

    public SessionNode() {
    }

    public SessionNode(
           com.orsyp.duws.SessionNode[] childKO,
           com.orsyp.duws.SessionNode[] childOK,
           boolean header,
           boolean leaf,
           int rankInSession,
           java.lang.String uprocName) {
           this.childKO = childKO;
           this.childOK = childOK;
           this.header = header;
           this.leaf = leaf;
           this.rankInSession = rankInSession;
           this.uprocName = uprocName;
    }


    /**
     * Gets the childKO value for this SessionNode.
     * 
     * @return childKO
     */
    public com.orsyp.duws.SessionNode[] getChildKO() {
        return childKO;
    }


    /**
     * Sets the childKO value for this SessionNode.
     * 
     * @param childKO
     */
    public void setChildKO(com.orsyp.duws.SessionNode[] childKO) {
        this.childKO = childKO;
    }

    public com.orsyp.duws.SessionNode getChildKO(int i) {
        return this.childKO[i];
    }

    public void setChildKO(int i, com.orsyp.duws.SessionNode _value) {
        this.childKO[i] = _value;
    }


    /**
     * Gets the childOK value for this SessionNode.
     * 
     * @return childOK
     */
    public com.orsyp.duws.SessionNode[] getChildOK() {
        return childOK;
    }


    /**
     * Sets the childOK value for this SessionNode.
     * 
     * @param childOK
     */
    public void setChildOK(com.orsyp.duws.SessionNode[] childOK) {
        this.childOK = childOK;
    }

    public com.orsyp.duws.SessionNode getChildOK(int i) {
        return this.childOK[i];
    }

    public void setChildOK(int i, com.orsyp.duws.SessionNode _value) {
        this.childOK[i] = _value;
    }


    /**
     * Gets the header value for this SessionNode.
     * 
     * @return header
     */
    public boolean isHeader() {
        return header;
    }


    /**
     * Sets the header value for this SessionNode.
     * 
     * @param header
     */
    public void setHeader(boolean header) {
        this.header = header;
    }


    /**
     * Gets the leaf value for this SessionNode.
     * 
     * @return leaf
     */
    public boolean isLeaf() {
        return leaf;
    }


    /**
     * Sets the leaf value for this SessionNode.
     * 
     * @param leaf
     */
    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }


    /**
     * Gets the rankInSession value for this SessionNode.
     * 
     * @return rankInSession
     */
    public int getRankInSession() {
        return rankInSession;
    }


    /**
     * Sets the rankInSession value for this SessionNode.
     * 
     * @param rankInSession
     */
    public void setRankInSession(int rankInSession) {
        this.rankInSession = rankInSession;
    }


    /**
     * Gets the uprocName value for this SessionNode.
     * 
     * @return uprocName
     */
    public java.lang.String getUprocName() {
        return uprocName;
    }


    /**
     * Sets the uprocName value for this SessionNode.
     * 
     * @param uprocName
     */
    public void setUprocName(java.lang.String uprocName) {
        this.uprocName = uprocName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SessionNode)) return false;
        SessionNode other = (SessionNode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.childKO==null && other.getChildKO()==null) || 
             (this.childKO!=null &&
              java.util.Arrays.equals(this.childKO, other.getChildKO()))) &&
            ((this.childOK==null && other.getChildOK()==null) || 
             (this.childOK!=null &&
              java.util.Arrays.equals(this.childOK, other.getChildOK()))) &&
            this.header == other.isHeader() &&
            this.leaf == other.isLeaf() &&
            this.rankInSession == other.getRankInSession() &&
            ((this.uprocName==null && other.getUprocName()==null) || 
             (this.uprocName!=null &&
              this.uprocName.equals(other.getUprocName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChildKO() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChildKO());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChildKO(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getChildOK() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChildOK());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChildOK(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isHeader() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isLeaf() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getRankInSession();
        if (getUprocName() != null) {
            _hashCode += getUprocName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SessionNode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionNode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childKO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "childKO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionNode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childOK");
        elemField.setXmlName(new javax.xml.namespace.QName("", "childOK"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionNode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("", "header"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leaf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "leaf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankInSession");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rankInSession"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uprocName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

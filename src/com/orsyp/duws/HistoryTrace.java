/**
 * HistoryTrace.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class HistoryTrace  implements java.io.Serializable {
    private com.orsyp.duws.ExecutionId ident;

    private java.lang.String uprocLabel;

    private java.lang.String sessionLabel;

    private java.lang.String muLabel;

    private java.lang.String[] trace;

    public HistoryTrace() {
    }

    public HistoryTrace(
           com.orsyp.duws.ExecutionId ident,
           java.lang.String uprocLabel,
           java.lang.String sessionLabel,
           java.lang.String muLabel,
           java.lang.String[] trace) {
           this.ident = ident;
           this.uprocLabel = uprocLabel;
           this.sessionLabel = sessionLabel;
           this.muLabel = muLabel;
           this.trace = trace;
    }


    /**
     * Gets the ident value for this HistoryTrace.
     * 
     * @return ident
     */
    public com.orsyp.duws.ExecutionId getIdent() {
        return ident;
    }


    /**
     * Sets the ident value for this HistoryTrace.
     * 
     * @param ident
     */
    public void setIdent(com.orsyp.duws.ExecutionId ident) {
        this.ident = ident;
    }


    /**
     * Gets the uprocLabel value for this HistoryTrace.
     * 
     * @return uprocLabel
     */
    public java.lang.String getUprocLabel() {
        return uprocLabel;
    }


    /**
     * Sets the uprocLabel value for this HistoryTrace.
     * 
     * @param uprocLabel
     */
    public void setUprocLabel(java.lang.String uprocLabel) {
        this.uprocLabel = uprocLabel;
    }


    /**
     * Gets the sessionLabel value for this HistoryTrace.
     * 
     * @return sessionLabel
     */
    public java.lang.String getSessionLabel() {
        return sessionLabel;
    }


    /**
     * Sets the sessionLabel value for this HistoryTrace.
     * 
     * @param sessionLabel
     */
    public void setSessionLabel(java.lang.String sessionLabel) {
        this.sessionLabel = sessionLabel;
    }


    /**
     * Gets the muLabel value for this HistoryTrace.
     * 
     * @return muLabel
     */
    public java.lang.String getMuLabel() {
        return muLabel;
    }


    /**
     * Sets the muLabel value for this HistoryTrace.
     * 
     * @param muLabel
     */
    public void setMuLabel(java.lang.String muLabel) {
        this.muLabel = muLabel;
    }


    /**
     * Gets the trace value for this HistoryTrace.
     * 
     * @return trace
     */
    public java.lang.String[] getTrace() {
        return trace;
    }


    /**
     * Sets the trace value for this HistoryTrace.
     * 
     * @param trace
     */
    public void setTrace(java.lang.String[] trace) {
        this.trace = trace;
    }

    public java.lang.String getTrace(int i) {
        return this.trace[i];
    }

    public void setTrace(int i, java.lang.String _value) {
        this.trace[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoryTrace)) return false;
        HistoryTrace other = (HistoryTrace) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ident==null && other.getIdent()==null) || 
             (this.ident!=null &&
              this.ident.equals(other.getIdent()))) &&
            ((this.uprocLabel==null && other.getUprocLabel()==null) || 
             (this.uprocLabel!=null &&
              this.uprocLabel.equals(other.getUprocLabel()))) &&
            ((this.sessionLabel==null && other.getSessionLabel()==null) || 
             (this.sessionLabel!=null &&
              this.sessionLabel.equals(other.getSessionLabel()))) &&
            ((this.muLabel==null && other.getMuLabel()==null) || 
             (this.muLabel!=null &&
              this.muLabel.equals(other.getMuLabel()))) &&
            ((this.trace==null && other.getTrace()==null) || 
             (this.trace!=null &&
              java.util.Arrays.equals(this.trace, other.getTrace())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdent() != null) {
            _hashCode += getIdent().hashCode();
        }
        if (getUprocLabel() != null) {
            _hashCode += getUprocLabel().hashCode();
        }
        if (getSessionLabel() != null) {
            _hashCode += getSessionLabel().hashCode();
        }
        if (getMuLabel() != null) {
            _hashCode += getMuLabel().hashCode();
        }
        if (getTrace() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTrace());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTrace(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoryTrace.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "historyTrace"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ident");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "ident"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "executionId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uprocLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uprocLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "sessionLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("muLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "muLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "trace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * OutageWindowHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws.holders;

public final class OutageWindowHolder implements javax.xml.rpc.holders.Holder {
    public com.orsyp.duws.OutageWindow value;

    public OutageWindowHolder() {
    }

    public OutageWindowHolder(com.orsyp.duws.OutageWindow value) {
        this.value = value;
    }

}

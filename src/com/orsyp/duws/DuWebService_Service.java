/**
 * DuWebService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public interface DuWebService_Service extends javax.xml.rpc.Service {
    public java.lang.String getDuWebServicePortAddress();

    public com.orsyp.duws.DuWebService_PortType getDuWebServicePort() throws javax.xml.rpc.ServiceException;

    public com.orsyp.duws.DuWebService_PortType getDuWebServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

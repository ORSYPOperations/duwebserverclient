package com.orsyp.duws;

public class DuWebServiceProxy implements com.orsyp.duws.DuWebService_PortType {
  private String _endpoint = null;
  private com.orsyp.duws.DuWebService_PortType duWebService_PortType = null;
  
  public DuWebServiceProxy() {
    _initDuWebServiceProxy();
  }
  
  public DuWebServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initDuWebServiceProxy();
  }
  
  private void _initDuWebServiceProxy() {
    try {
      duWebService_PortType = (new com.orsyp.duws.DuWebService_ServiceLocator()).getDuWebServicePort();
      if (duWebService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)duWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)duWebService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (duWebService_PortType != null)
      ((javax.xml.rpc.Stub)duWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.orsyp.duws.DuWebService_PortType getDuWebService_PortType() {
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType;
  }
  
  public com.orsyp.duws.UprocItem[] getListUproc(com.orsyp.duws.ContextHolder context, com.orsyp.duws.UprocFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListUproc(context, filter);
  }
  
  public void forceCompleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.forceCompleteLaunch(context, launchId);
  }
  
  public void deleteEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.deleteEvent(context, eventId);
  }
  
  public com.orsyp.duws.SessionItem[] getListSession(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListSession(context, filter);
  }
  
  public com.orsyp.duws.Launch getLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getLaunch(context, launchId);
  }
  
  public void restartEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.restartEngine(context, engine);
  }
  
  public com.orsyp.duws.LaunchId addLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.addLaunch(context, launch);
  }
  
  public com.orsyp.duws.RunBook[] getRunBooks(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBookFilter runBookFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getRunBooks(context, runBookFilter);
  }
  
  public com.orsyp.duws.EventItem[] getListEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListEvent(context, filter);
  }
  
  public com.orsyp.duws.PreviousLaunch[] getPreviousLaunches(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getPreviousLaunches(context, executionId);
  }
  
  public com.orsyp.duws.LaunchId addLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.addLaunchFromTask(context, taskId);
  }
  
  public void stopQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.stopQueue(context, queue);
  }
  
  public void logout(java.lang.String token) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.logout(token);
  }
  
  public void enableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.enableLaunch(context, launchId);
  }
  
  public com.orsyp.duws.MuItem[] getListMU(com.orsyp.duws.ContextHolder context, com.orsyp.duws.MuFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListMU(context, filter);
  }
  
  public void startEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.startEngine(context, engine);
  }
  
  public com.orsyp.duws.ExecutionLog getExecutionLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getExecutionLog(context, executionId);
  }
  
  public void updateEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId, java.lang.String status, java.lang.String step) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.updateEvent(context, eventId, status, step);
  }
  
  public com.orsyp.duws.Execution getExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getExecution(context, executionId);
  }
  
  public void updateLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.updateLaunch(context, launch);
  }
  
  public void resetQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.resetQueue(context, queue);
  }
  
  public byte[] getRunBookExternalFile(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBook runBook) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getRunBookExternalFile(context, runBook);
  }
  
  public void createOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.holders.OutageWindowHolder outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.createOutageWindow(context, outage);
  }
  
  public com.orsyp.duws.LaunchItem[] getListLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListLaunch(context, filter);
  }
  
  public void holdLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.holdLaunch(context, launch);
  }
  
  public com.orsyp.duws.RunNote[] getRunNotes(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunNoteFilter noteFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getRunNotes(context, noteFilter);
  }
  
  public com.orsyp.duws.Event getEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getEvent(context, eventId);
  }
  
  public com.orsyp.duws.OutageWindow[] getListOutage(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageFilter outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListOutage(context, outage);
  }
  
  public byte[] getExecutionLogAsAttachment(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getExecutionLogAsAttachment(context, executionId);
  }
  
  public void addEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Event event) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.addEvent(context, event);
  }
  
  public com.orsyp.duws.LaunchId addLaunchFromTask2(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId, com.orsyp.duws.Variable[] variables) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.addLaunchFromTask2(context, taskId, variables);
  }
  
  public com.orsyp.duws.Launch getLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getLaunchFromTask(context, taskId);
  }
  
  public void purgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.purgeExecution(context, executionId);
  }
  
  public void deleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.deleteLaunch(context, launchId);
  }
  
  public com.orsyp.duws.NodeItem[] getListNode(com.orsyp.duws.ContextHolder context, com.orsyp.duws.NodeFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListNode(context, filter);
  }
  
  public com.orsyp.duws.HistoryTrace getHistoryTrace(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getHistoryTrace(context, executionId);
  }
  
  public void skipExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.skipExecution(context, launchId);
  }
  
  public void rerunExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, java.lang.String startDate, java.lang.String endDate, java.lang.String batchQueue, java.lang.String user, int step, boolean byPassCheck) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.rerunExecution(context, executionId, startDate, endDate, batchQueue, user, step, byPassCheck);
  }
  
  public void releaseLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.releaseLaunch(context, launchId);
  }
  
  public com.orsyp.duws.ResourceLog getScriptResourceLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getScriptResourceLog(context, executionId);
  }
  
  public void stopExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, int delay) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.stopExecution(context, executionId, delay);
  }
  
  public void stopEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.stopEngine(context, engine);
  }
  
  public com.orsyp.duws.Envir[] getDUEnvironmentList(java.lang.String token, com.orsyp.duws.UvmsNodeFilter nodeFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getDUEnvironmentList(token, nodeFilter);
  }
  
  public com.orsyp.duws.ExecutionItem[] getListExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListExecution(context, filter);
  }
  
  public void bypassLaunchConditionCheck(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.bypassLaunchConditionCheck(context, launchId);
  }
  
  public void deleteOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageWindow outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.deleteOutageWindow(context, outage);
  }
  
  public void unacknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.unacknowledgeExecution(context, launchId);
  }
  
  public void acknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.acknowledgeExecution(context, launchId);
  }
  
  public com.orsyp.duws.DuwsVersion getWsVersion() throws java.rmi.RemoteException, com.orsyp.duws.DuwsException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getWsVersion();
  }
  
  public void disableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.disableLaunch(context, launchId);
  }
  
  public com.orsyp.duws.SessionTree getSessionTree(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionItem sessionItem) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getSessionTree(context, sessionItem);
  }
  
  public com.orsyp.duws.TaskItem[] getListTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.getListTask(context, filter);
  }
  
  public void startQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    duWebService_PortType.startQueue(context, queue);
  }
  
  public java.lang.String login(com.orsyp.duws.UvmsContext uvms) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException{
    if (duWebService_PortType == null)
      _initDuWebServiceProxy();
    return duWebService_PortType.login(uvms);
  }
  
  
}
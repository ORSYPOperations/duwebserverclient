/**
 * DuwsVersion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class DuwsVersion  implements java.io.Serializable {
    private int version;

    private int build;

    private java.lang.String fullVersion;

    public DuwsVersion() {
    }

    public DuwsVersion(
           int version,
           int build,
           java.lang.String fullVersion) {
           this.version = version;
           this.build = build;
           this.fullVersion = fullVersion;
    }


    /**
     * Gets the version value for this DuwsVersion.
     * 
     * @return version
     */
    public int getVersion() {
        return version;
    }


    /**
     * Sets the version value for this DuwsVersion.
     * 
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }


    /**
     * Gets the build value for this DuwsVersion.
     * 
     * @return build
     */
    public int getBuild() {
        return build;
    }


    /**
     * Sets the build value for this DuwsVersion.
     * 
     * @param build
     */
    public void setBuild(int build) {
        this.build = build;
    }


    /**
     * Gets the fullVersion value for this DuwsVersion.
     * 
     * @return fullVersion
     */
    public java.lang.String getFullVersion() {
        return fullVersion;
    }


    /**
     * Sets the fullVersion value for this DuwsVersion.
     * 
     * @param fullVersion
     */
    public void setFullVersion(java.lang.String fullVersion) {
        this.fullVersion = fullVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DuwsVersion)) return false;
        DuwsVersion other = (DuwsVersion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.version == other.getVersion() &&
            this.build == other.getBuild() &&
            ((this.fullVersion==null && other.getFullVersion()==null) || 
             (this.fullVersion!=null &&
              this.fullVersion.equals(other.getFullVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getVersion();
        _hashCode += getBuild();
        if (getFullVersion() != null) {
            _hashCode += getFullVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DuwsVersion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "duwsVersion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("build");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "build"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "fullVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * DuWebService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class DuWebService_ServiceLocator extends org.apache.axis.client.Service implements com.orsyp.duws.DuWebService_Service {

    public DuWebService_ServiceLocator() {
    }


    public DuWebService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DuWebService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DuWebServicePort
    private java.lang.String DuWebServicePort_address;

    public void setDuWebServicePortAddress(String setWebServicesLink) {
        DuWebServicePort_address=setWebServicesLink;
    }
    
    public java.lang.String getDuWebServicePortAddress() {
        return DuWebServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DuWebServicePortWSDDServiceName = "DuWebServicePort";

    public java.lang.String getDuWebServicePortWSDDServiceName() {
        return DuWebServicePortWSDDServiceName;
    }

    public void setDuWebServicePortWSDDServiceName(java.lang.String name) {
        DuWebServicePortWSDDServiceName = name;
    }

    public com.orsyp.duws.DuWebService_PortType getDuWebServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DuWebServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDuWebServicePort(endpoint);
    }

    public com.orsyp.duws.DuWebService_PortType getDuWebServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.orsyp.duws.DuWebServiceSoapBindingStub _stub = new com.orsyp.duws.DuWebServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getDuWebServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDuWebServicePortEndpointAddress(java.lang.String address) {
        DuWebServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.orsyp.duws.DuWebService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.orsyp.duws.DuWebServiceSoapBindingStub _stub = new com.orsyp.duws.DuWebServiceSoapBindingStub(new java.net.URL(DuWebServicePort_address), this);
                _stub.setPortName(getDuWebServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DuWebServicePort".equals(inputPortName)) {
            return getDuWebServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://duws.orsyp.com", "DuWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://duws.orsyp.com", "DuWebServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DuWebServicePort".equals(portName)) {
            setDuWebServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

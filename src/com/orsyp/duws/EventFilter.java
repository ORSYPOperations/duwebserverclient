/**
 * EventFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class EventFilter  implements java.io.Serializable {
    private java.lang.String uproc;

    private java.lang.String session;

    private java.lang.String mu;

    private java.lang.String status;

    private java.lang.String user;

    private java.lang.String creationDateMin;

    private java.lang.String creationDateMax;

    private java.lang.String creationHourMin;

    private java.lang.String creationHourMax;

    private java.lang.String updateDateMin;

    private java.lang.String updateDateMax;

    private java.lang.String updateHourMin;

    private java.lang.String updateHourMax;

    private java.lang.String processingDate;

    private java.lang.String numlancMin;

    private java.lang.String numlancMax;

    private java.lang.String numsessMin;

    private java.lang.String numsessMax;

    private java.lang.String numprocMin;

    private java.lang.String numprocMax;

    public EventFilter() {
    }

    public EventFilter(
           java.lang.String uproc,
           java.lang.String session,
           java.lang.String mu,
           java.lang.String status,
           java.lang.String user,
           java.lang.String creationDateMin,
           java.lang.String creationDateMax,
           java.lang.String creationHourMin,
           java.lang.String creationHourMax,
           java.lang.String updateDateMin,
           java.lang.String updateDateMax,
           java.lang.String updateHourMin,
           java.lang.String updateHourMax,
           java.lang.String processingDate,
           java.lang.String numlancMin,
           java.lang.String numlancMax,
           java.lang.String numsessMin,
           java.lang.String numsessMax,
           java.lang.String numprocMin,
           java.lang.String numprocMax) {
           this.uproc = uproc;
           this.session = session;
           this.mu = mu;
           this.status = status;
           this.user = user;
           this.creationDateMin = creationDateMin;
           this.creationDateMax = creationDateMax;
           this.creationHourMin = creationHourMin;
           this.creationHourMax = creationHourMax;
           this.updateDateMin = updateDateMin;
           this.updateDateMax = updateDateMax;
           this.updateHourMin = updateHourMin;
           this.updateHourMax = updateHourMax;
           this.processingDate = processingDate;
           this.numlancMin = numlancMin;
           this.numlancMax = numlancMax;
           this.numsessMin = numsessMin;
           this.numsessMax = numsessMax;
           this.numprocMin = numprocMin;
           this.numprocMax = numprocMax;
    }


    /**
     * Gets the uproc value for this EventFilter.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this EventFilter.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the session value for this EventFilter.
     * 
     * @return session
     */
    public java.lang.String getSession() {
        return session;
    }


    /**
     * Sets the session value for this EventFilter.
     * 
     * @param session
     */
    public void setSession(java.lang.String session) {
        this.session = session;
    }


    /**
     * Gets the mu value for this EventFilter.
     * 
     * @return mu
     */
    public java.lang.String getMu() {
        return mu;
    }


    /**
     * Sets the mu value for this EventFilter.
     * 
     * @param mu
     */
    public void setMu(java.lang.String mu) {
        this.mu = mu;
    }


    /**
     * Gets the status value for this EventFilter.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this EventFilter.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the user value for this EventFilter.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this EventFilter.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }


    /**
     * Gets the creationDateMin value for this EventFilter.
     * 
     * @return creationDateMin
     */
    public java.lang.String getCreationDateMin() {
        return creationDateMin;
    }


    /**
     * Sets the creationDateMin value for this EventFilter.
     * 
     * @param creationDateMin
     */
    public void setCreationDateMin(java.lang.String creationDateMin) {
        this.creationDateMin = creationDateMin;
    }


    /**
     * Gets the creationDateMax value for this EventFilter.
     * 
     * @return creationDateMax
     */
    public java.lang.String getCreationDateMax() {
        return creationDateMax;
    }


    /**
     * Sets the creationDateMax value for this EventFilter.
     * 
     * @param creationDateMax
     */
    public void setCreationDateMax(java.lang.String creationDateMax) {
        this.creationDateMax = creationDateMax;
    }


    /**
     * Gets the creationHourMin value for this EventFilter.
     * 
     * @return creationHourMin
     */
    public java.lang.String getCreationHourMin() {
        return creationHourMin;
    }


    /**
     * Sets the creationHourMin value for this EventFilter.
     * 
     * @param creationHourMin
     */
    public void setCreationHourMin(java.lang.String creationHourMin) {
        this.creationHourMin = creationHourMin;
    }


    /**
     * Gets the creationHourMax value for this EventFilter.
     * 
     * @return creationHourMax
     */
    public java.lang.String getCreationHourMax() {
        return creationHourMax;
    }


    /**
     * Sets the creationHourMax value for this EventFilter.
     * 
     * @param creationHourMax
     */
    public void setCreationHourMax(java.lang.String creationHourMax) {
        this.creationHourMax = creationHourMax;
    }


    /**
     * Gets the updateDateMin value for this EventFilter.
     * 
     * @return updateDateMin
     */
    public java.lang.String getUpdateDateMin() {
        return updateDateMin;
    }


    /**
     * Sets the updateDateMin value for this EventFilter.
     * 
     * @param updateDateMin
     */
    public void setUpdateDateMin(java.lang.String updateDateMin) {
        this.updateDateMin = updateDateMin;
    }


    /**
     * Gets the updateDateMax value for this EventFilter.
     * 
     * @return updateDateMax
     */
    public java.lang.String getUpdateDateMax() {
        return updateDateMax;
    }


    /**
     * Sets the updateDateMax value for this EventFilter.
     * 
     * @param updateDateMax
     */
    public void setUpdateDateMax(java.lang.String updateDateMax) {
        this.updateDateMax = updateDateMax;
    }


    /**
     * Gets the updateHourMin value for this EventFilter.
     * 
     * @return updateHourMin
     */
    public java.lang.String getUpdateHourMin() {
        return updateHourMin;
    }


    /**
     * Sets the updateHourMin value for this EventFilter.
     * 
     * @param updateHourMin
     */
    public void setUpdateHourMin(java.lang.String updateHourMin) {
        this.updateHourMin = updateHourMin;
    }


    /**
     * Gets the updateHourMax value for this EventFilter.
     * 
     * @return updateHourMax
     */
    public java.lang.String getUpdateHourMax() {
        return updateHourMax;
    }


    /**
     * Sets the updateHourMax value for this EventFilter.
     * 
     * @param updateHourMax
     */
    public void setUpdateHourMax(java.lang.String updateHourMax) {
        this.updateHourMax = updateHourMax;
    }


    /**
     * Gets the processingDate value for this EventFilter.
     * 
     * @return processingDate
     */
    public java.lang.String getProcessingDate() {
        return processingDate;
    }


    /**
     * Sets the processingDate value for this EventFilter.
     * 
     * @param processingDate
     */
    public void setProcessingDate(java.lang.String processingDate) {
        this.processingDate = processingDate;
    }


    /**
     * Gets the numlancMin value for this EventFilter.
     * 
     * @return numlancMin
     */
    public java.lang.String getNumlancMin() {
        return numlancMin;
    }


    /**
     * Sets the numlancMin value for this EventFilter.
     * 
     * @param numlancMin
     */
    public void setNumlancMin(java.lang.String numlancMin) {
        this.numlancMin = numlancMin;
    }


    /**
     * Gets the numlancMax value for this EventFilter.
     * 
     * @return numlancMax
     */
    public java.lang.String getNumlancMax() {
        return numlancMax;
    }


    /**
     * Sets the numlancMax value for this EventFilter.
     * 
     * @param numlancMax
     */
    public void setNumlancMax(java.lang.String numlancMax) {
        this.numlancMax = numlancMax;
    }


    /**
     * Gets the numsessMin value for this EventFilter.
     * 
     * @return numsessMin
     */
    public java.lang.String getNumsessMin() {
        return numsessMin;
    }


    /**
     * Sets the numsessMin value for this EventFilter.
     * 
     * @param numsessMin
     */
    public void setNumsessMin(java.lang.String numsessMin) {
        this.numsessMin = numsessMin;
    }


    /**
     * Gets the numsessMax value for this EventFilter.
     * 
     * @return numsessMax
     */
    public java.lang.String getNumsessMax() {
        return numsessMax;
    }


    /**
     * Sets the numsessMax value for this EventFilter.
     * 
     * @param numsessMax
     */
    public void setNumsessMax(java.lang.String numsessMax) {
        this.numsessMax = numsessMax;
    }


    /**
     * Gets the numprocMin value for this EventFilter.
     * 
     * @return numprocMin
     */
    public java.lang.String getNumprocMin() {
        return numprocMin;
    }


    /**
     * Sets the numprocMin value for this EventFilter.
     * 
     * @param numprocMin
     */
    public void setNumprocMin(java.lang.String numprocMin) {
        this.numprocMin = numprocMin;
    }


    /**
     * Gets the numprocMax value for this EventFilter.
     * 
     * @return numprocMax
     */
    public java.lang.String getNumprocMax() {
        return numprocMax;
    }


    /**
     * Sets the numprocMax value for this EventFilter.
     * 
     * @param numprocMax
     */
    public void setNumprocMax(java.lang.String numprocMax) {
        this.numprocMax = numprocMax;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventFilter)) return false;
        EventFilter other = (EventFilter) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.session==null && other.getSession()==null) || 
             (this.session!=null &&
              this.session.equals(other.getSession()))) &&
            ((this.mu==null && other.getMu()==null) || 
             (this.mu!=null &&
              this.mu.equals(other.getMu()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.creationDateMin==null && other.getCreationDateMin()==null) || 
             (this.creationDateMin!=null &&
              this.creationDateMin.equals(other.getCreationDateMin()))) &&
            ((this.creationDateMax==null && other.getCreationDateMax()==null) || 
             (this.creationDateMax!=null &&
              this.creationDateMax.equals(other.getCreationDateMax()))) &&
            ((this.creationHourMin==null && other.getCreationHourMin()==null) || 
             (this.creationHourMin!=null &&
              this.creationHourMin.equals(other.getCreationHourMin()))) &&
            ((this.creationHourMax==null && other.getCreationHourMax()==null) || 
             (this.creationHourMax!=null &&
              this.creationHourMax.equals(other.getCreationHourMax()))) &&
            ((this.updateDateMin==null && other.getUpdateDateMin()==null) || 
             (this.updateDateMin!=null &&
              this.updateDateMin.equals(other.getUpdateDateMin()))) &&
            ((this.updateDateMax==null && other.getUpdateDateMax()==null) || 
             (this.updateDateMax!=null &&
              this.updateDateMax.equals(other.getUpdateDateMax()))) &&
            ((this.updateHourMin==null && other.getUpdateHourMin()==null) || 
             (this.updateHourMin!=null &&
              this.updateHourMin.equals(other.getUpdateHourMin()))) &&
            ((this.updateHourMax==null && other.getUpdateHourMax()==null) || 
             (this.updateHourMax!=null &&
              this.updateHourMax.equals(other.getUpdateHourMax()))) &&
            ((this.processingDate==null && other.getProcessingDate()==null) || 
             (this.processingDate!=null &&
              this.processingDate.equals(other.getProcessingDate()))) &&
            ((this.numlancMin==null && other.getNumlancMin()==null) || 
             (this.numlancMin!=null &&
              this.numlancMin.equals(other.getNumlancMin()))) &&
            ((this.numlancMax==null && other.getNumlancMax()==null) || 
             (this.numlancMax!=null &&
              this.numlancMax.equals(other.getNumlancMax()))) &&
            ((this.numsessMin==null && other.getNumsessMin()==null) || 
             (this.numsessMin!=null &&
              this.numsessMin.equals(other.getNumsessMin()))) &&
            ((this.numsessMax==null && other.getNumsessMax()==null) || 
             (this.numsessMax!=null &&
              this.numsessMax.equals(other.getNumsessMax()))) &&
            ((this.numprocMin==null && other.getNumprocMin()==null) || 
             (this.numprocMin!=null &&
              this.numprocMin.equals(other.getNumprocMin()))) &&
            ((this.numprocMax==null && other.getNumprocMax()==null) || 
             (this.numprocMax!=null &&
              this.numprocMax.equals(other.getNumprocMax())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getSession() != null) {
            _hashCode += getSession().hashCode();
        }
        if (getMu() != null) {
            _hashCode += getMu().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getCreationDateMin() != null) {
            _hashCode += getCreationDateMin().hashCode();
        }
        if (getCreationDateMax() != null) {
            _hashCode += getCreationDateMax().hashCode();
        }
        if (getCreationHourMin() != null) {
            _hashCode += getCreationHourMin().hashCode();
        }
        if (getCreationHourMax() != null) {
            _hashCode += getCreationHourMax().hashCode();
        }
        if (getUpdateDateMin() != null) {
            _hashCode += getUpdateDateMin().hashCode();
        }
        if (getUpdateDateMax() != null) {
            _hashCode += getUpdateDateMax().hashCode();
        }
        if (getUpdateHourMin() != null) {
            _hashCode += getUpdateHourMin().hashCode();
        }
        if (getUpdateHourMax() != null) {
            _hashCode += getUpdateHourMax().hashCode();
        }
        if (getProcessingDate() != null) {
            _hashCode += getProcessingDate().hashCode();
        }
        if (getNumlancMin() != null) {
            _hashCode += getNumlancMin().hashCode();
        }
        if (getNumlancMax() != null) {
            _hashCode += getNumlancMax().hashCode();
        }
        if (getNumsessMin() != null) {
            _hashCode += getNumsessMin().hashCode();
        }
        if (getNumsessMax() != null) {
            _hashCode += getNumsessMax().hashCode();
        }
        if (getNumprocMin() != null) {
            _hashCode += getNumprocMin().hashCode();
        }
        if (getNumprocMax() != null) {
            _hashCode += getNumprocMax().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventFilter.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventFilter"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("session");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "session"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "mu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDateMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationDateMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDateMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationDateMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationHourMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationHourMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationHourMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationHourMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDateMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateDateMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDateMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateDateMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateHourMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateHourMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateHourMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateHourMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "processingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numlancMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numlancMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numlancMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numlancMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numsessMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numsessMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numsessMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numsessMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numprocMin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numprocMin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numprocMax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numprocMax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

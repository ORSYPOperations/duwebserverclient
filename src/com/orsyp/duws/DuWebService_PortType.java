/**
 * DuWebService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public interface DuWebService_PortType extends java.rmi.Remote {
    public com.orsyp.duws.UprocItem[] getListUproc(com.orsyp.duws.ContextHolder context, com.orsyp.duws.UprocFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void forceCompleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void deleteEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.SessionItem[] getListSession(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.Launch getLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void restartEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.LaunchId addLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.RunBook[] getRunBooks(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBookFilter runBookFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.EventItem[] getListEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.PreviousLaunch[] getPreviousLaunches(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.LaunchId addLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void stopQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void logout(java.lang.String token) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException;
    public void enableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.MuItem[] getListMU(com.orsyp.duws.ContextHolder context, com.orsyp.duws.MuFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void startEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.ExecutionLog getExecutionLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void updateEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId, java.lang.String status, java.lang.String step) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.Execution getExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void updateLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Launch launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void resetQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public byte[] getRunBookExternalFile(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunBook runBook) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void createOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.holders.OutageWindowHolder outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.LaunchItem[] getListLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void holdLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launch) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.RunNote[] getRunNotes(com.orsyp.duws.ContextHolder context, com.orsyp.duws.RunNoteFilter noteFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.Event getEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.EventId eventId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.OutageWindow[] getListOutage(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageFilter outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public byte[] getExecutionLogAsAttachment(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void addEvent(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Event event) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.LaunchId addLaunchFromTask2(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId, com.orsyp.duws.Variable[] variables) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.Launch getLaunchFromTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskId taskId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void purgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void deleteLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.NodeItem[] getListNode(com.orsyp.duws.ContextHolder context, com.orsyp.duws.NodeFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.HistoryTrace getHistoryTrace(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void skipExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void rerunExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, java.lang.String startDate, java.lang.String endDate, java.lang.String batchQueue, java.lang.String user, int step, boolean byPassCheck) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void releaseLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.ResourceLog getScriptResourceLog(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void stopExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionId executionId, int delay) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void stopEngine(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Engine engine) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.Envir[] getDUEnvironmentList(java.lang.String token, com.orsyp.duws.UvmsNodeFilter nodeFilter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.ExecutionItem[] getListExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.ExecutionFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void bypassLaunchConditionCheck(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void deleteOutageWindow(com.orsyp.duws.ContextHolder context, com.orsyp.duws.OutageWindow outage) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void unacknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void acknowledgeExecution(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.DuwsVersion getWsVersion() throws java.rmi.RemoteException, com.orsyp.duws.DuwsException;
    public void disableLaunch(com.orsyp.duws.ContextHolder context, com.orsyp.duws.LaunchId launchId) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.SessionTree getSessionTree(com.orsyp.duws.ContextHolder context, com.orsyp.duws.SessionItem sessionItem) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public com.orsyp.duws.TaskItem[] getListTask(com.orsyp.duws.ContextHolder context, com.orsyp.duws.TaskFilter filter) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public void startQueue(com.orsyp.duws.ContextHolder context, com.orsyp.duws.Queue queue) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException, com.orsyp.duws.SessionTimedOutException;
    public java.lang.String login(com.orsyp.duws.UvmsContext uvms) throws java.rmi.RemoteException, com.orsyp.duws.DuwsException;
}

/**
 * EventItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class EventItem  implements java.io.Serializable {
    private com.orsyp.duws.EventId ident;

    private java.lang.String status;

    private java.lang.String step;

    private java.lang.String authorCode;

    private java.lang.String numLanc;

    private java.lang.String creationDate;

    private java.lang.String creationHour;

    private java.lang.String updateDate;

    private java.lang.String updateHour;

    public EventItem() {
    }

    public EventItem(
           com.orsyp.duws.EventId ident,
           java.lang.String status,
           java.lang.String step,
           java.lang.String authorCode,
           java.lang.String numLanc,
           java.lang.String creationDate,
           java.lang.String creationHour,
           java.lang.String updateDate,
           java.lang.String updateHour) {
           this.ident = ident;
           this.status = status;
           this.step = step;
           this.authorCode = authorCode;
           this.numLanc = numLanc;
           this.creationDate = creationDate;
           this.creationHour = creationHour;
           this.updateDate = updateDate;
           this.updateHour = updateHour;
    }


    /**
     * Gets the ident value for this EventItem.
     * 
     * @return ident
     */
    public com.orsyp.duws.EventId getIdent() {
        return ident;
    }


    /**
     * Sets the ident value for this EventItem.
     * 
     * @param ident
     */
    public void setIdent(com.orsyp.duws.EventId ident) {
        this.ident = ident;
    }


    /**
     * Gets the status value for this EventItem.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this EventItem.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the step value for this EventItem.
     * 
     * @return step
     */
    public java.lang.String getStep() {
        return step;
    }


    /**
     * Sets the step value for this EventItem.
     * 
     * @param step
     */
    public void setStep(java.lang.String step) {
        this.step = step;
    }


    /**
     * Gets the authorCode value for this EventItem.
     * 
     * @return authorCode
     */
    public java.lang.String getAuthorCode() {
        return authorCode;
    }


    /**
     * Sets the authorCode value for this EventItem.
     * 
     * @param authorCode
     */
    public void setAuthorCode(java.lang.String authorCode) {
        this.authorCode = authorCode;
    }


    /**
     * Gets the numLanc value for this EventItem.
     * 
     * @return numLanc
     */
    public java.lang.String getNumLanc() {
        return numLanc;
    }


    /**
     * Sets the numLanc value for this EventItem.
     * 
     * @param numLanc
     */
    public void setNumLanc(java.lang.String numLanc) {
        this.numLanc = numLanc;
    }


    /**
     * Gets the creationDate value for this EventItem.
     * 
     * @return creationDate
     */
    public java.lang.String getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this EventItem.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.lang.String creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the creationHour value for this EventItem.
     * 
     * @return creationHour
     */
    public java.lang.String getCreationHour() {
        return creationHour;
    }


    /**
     * Sets the creationHour value for this EventItem.
     * 
     * @param creationHour
     */
    public void setCreationHour(java.lang.String creationHour) {
        this.creationHour = creationHour;
    }


    /**
     * Gets the updateDate value for this EventItem.
     * 
     * @return updateDate
     */
    public java.lang.String getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the updateDate value for this EventItem.
     * 
     * @param updateDate
     */
    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }


    /**
     * Gets the updateHour value for this EventItem.
     * 
     * @return updateHour
     */
    public java.lang.String getUpdateHour() {
        return updateHour;
    }


    /**
     * Sets the updateHour value for this EventItem.
     * 
     * @param updateHour
     */
    public void setUpdateHour(java.lang.String updateHour) {
        this.updateHour = updateHour;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EventItem)) return false;
        EventItem other = (EventItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ident==null && other.getIdent()==null) || 
             (this.ident!=null &&
              this.ident.equals(other.getIdent()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.step==null && other.getStep()==null) || 
             (this.step!=null &&
              this.step.equals(other.getStep()))) &&
            ((this.authorCode==null && other.getAuthorCode()==null) || 
             (this.authorCode!=null &&
              this.authorCode.equals(other.getAuthorCode()))) &&
            ((this.numLanc==null && other.getNumLanc()==null) || 
             (this.numLanc!=null &&
              this.numLanc.equals(other.getNumLanc()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.creationHour==null && other.getCreationHour()==null) || 
             (this.creationHour!=null &&
              this.creationHour.equals(other.getCreationHour()))) &&
            ((this.updateDate==null && other.getUpdateDate()==null) || 
             (this.updateDate!=null &&
              this.updateDate.equals(other.getUpdateDate()))) &&
            ((this.updateHour==null && other.getUpdateHour()==null) || 
             (this.updateHour!=null &&
              this.updateHour.equals(other.getUpdateHour())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdent() != null) {
            _hashCode += getIdent().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStep() != null) {
            _hashCode += getStep().hashCode();
        }
        if (getAuthorCode() != null) {
            _hashCode += getAuthorCode().hashCode();
        }
        if (getNumLanc() != null) {
            _hashCode += getNumLanc().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getCreationHour() != null) {
            _hashCode += getCreationHour().hashCode();
        }
        if (getUpdateDate() != null) {
            _hashCode += getUpdateDate().hashCode();
        }
        if (getUpdateHour() != null) {
            _hashCode += getUpdateHour().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EventItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ident");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "ident"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "eventId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("step");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "step"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "authorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numLanc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numLanc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationHour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "creationHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateHour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "updateHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

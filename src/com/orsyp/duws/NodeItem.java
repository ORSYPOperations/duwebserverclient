/**
 * NodeItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class NodeItem  implements java.io.Serializable {
    private com.orsyp.duws.NodeId ident;

    private java.lang.String label;

    private boolean development;

    private boolean production;

    private boolean centralControl;

    public NodeItem() {
    }

    public NodeItem(
           com.orsyp.duws.NodeId ident,
           java.lang.String label,
           boolean development,
           boolean production,
           boolean centralControl) {
           this.ident = ident;
           this.label = label;
           this.development = development;
           this.production = production;
           this.centralControl = centralControl;
    }


    /**
     * Gets the ident value for this NodeItem.
     * 
     * @return ident
     */
    public com.orsyp.duws.NodeId getIdent() {
        return ident;
    }


    /**
     * Sets the ident value for this NodeItem.
     * 
     * @param ident
     */
    public void setIdent(com.orsyp.duws.NodeId ident) {
        this.ident = ident;
    }


    /**
     * Gets the label value for this NodeItem.
     * 
     * @return label
     */
    public java.lang.String getLabel() {
        return label;
    }


    /**
     * Sets the label value for this NodeItem.
     * 
     * @param label
     */
    public void setLabel(java.lang.String label) {
        this.label = label;
    }


    /**
     * Gets the development value for this NodeItem.
     * 
     * @return development
     */
    public boolean isDevelopment() {
        return development;
    }


    /**
     * Sets the development value for this NodeItem.
     * 
     * @param development
     */
    public void setDevelopment(boolean development) {
        this.development = development;
    }


    /**
     * Gets the production value for this NodeItem.
     * 
     * @return production
     */
    public boolean isProduction() {
        return production;
    }


    /**
     * Sets the production value for this NodeItem.
     * 
     * @param production
     */
    public void setProduction(boolean production) {
        this.production = production;
    }


    /**
     * Gets the centralControl value for this NodeItem.
     * 
     * @return centralControl
     */
    public boolean isCentralControl() {
        return centralControl;
    }


    /**
     * Sets the centralControl value for this NodeItem.
     * 
     * @param centralControl
     */
    public void setCentralControl(boolean centralControl) {
        this.centralControl = centralControl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NodeItem)) return false;
        NodeItem other = (NodeItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ident==null && other.getIdent()==null) || 
             (this.ident!=null &&
              this.ident.equals(other.getIdent()))) &&
            ((this.label==null && other.getLabel()==null) || 
             (this.label!=null &&
              this.label.equals(other.getLabel()))) &&
            this.development == other.isDevelopment() &&
            this.production == other.isProduction() &&
            this.centralControl == other.isCentralControl();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdent() != null) {
            _hashCode += getIdent().hashCode();
        }
        if (getLabel() != null) {
            _hashCode += getLabel().hashCode();
        }
        _hashCode += (isDevelopment() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isProduction() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isCentralControl() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NodeItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ident");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "ident"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "nodeId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("label");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "label"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("development");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "development"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("production");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "production"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("centralControl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "centralControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

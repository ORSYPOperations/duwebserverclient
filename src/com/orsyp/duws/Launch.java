/**
 * Launch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class Launch  implements java.io.Serializable {
    private com.orsyp.duws.LaunchId ident;

    private com.orsyp.duws.Variable[] variables;

    private boolean autoRestart;

    private boolean centralControl;

    private boolean forcedExecution;

    private boolean bypassCondCheck;

    private java.lang.String exclusionFrom;

    private java.lang.String exclusionTo;

    private java.lang.String status;

    private java.lang.String beginDate;

    private java.lang.String beginHour;

    private java.lang.String endDate;

    private java.lang.String endHour;

    private java.lang.String processingDate;

    private java.lang.String user;

    private java.lang.String step;

    private java.lang.String node;

    private java.lang.String queue;

    private java.lang.String priority;

    private java.lang.String information;

    private int severity;

    private int rankInSession;

    public Launch() {
    }

    public Launch(
           com.orsyp.duws.LaunchId ident,
           com.orsyp.duws.Variable[] variables,
           boolean autoRestart,
           boolean centralControl,
           boolean forcedExecution,
           boolean bypassCondCheck,
           java.lang.String exclusionFrom,
           java.lang.String exclusionTo,
           java.lang.String status,
           java.lang.String beginDate,
           java.lang.String beginHour,
           java.lang.String endDate,
           java.lang.String endHour,
           java.lang.String processingDate,
           java.lang.String user,
           java.lang.String step,
           java.lang.String node,
           java.lang.String queue,
           java.lang.String priority,
           java.lang.String information,
           int severity,
           int rankInSession) {
           this.ident = ident;
           this.variables = variables;
           this.autoRestart = autoRestart;
           this.centralControl = centralControl;
           this.forcedExecution = forcedExecution;
           this.bypassCondCheck = bypassCondCheck;
           this.exclusionFrom = exclusionFrom;
           this.exclusionTo = exclusionTo;
           this.status = status;
           this.beginDate = beginDate;
           this.beginHour = beginHour;
           this.endDate = endDate;
           this.endHour = endHour;
           this.processingDate = processingDate;
           this.user = user;
           this.step = step;
           this.node = node;
           this.queue = queue;
           this.priority = priority;
           this.information = information;
           this.severity = severity;
           this.rankInSession = rankInSession;
    }


    /**
     * Gets the ident value for this Launch.
     * 
     * @return ident
     */
    public com.orsyp.duws.LaunchId getIdent() {
        return ident;
    }


    /**
     * Sets the ident value for this Launch.
     * 
     * @param ident
     */
    public void setIdent(com.orsyp.duws.LaunchId ident) {
        this.ident = ident;
    }


    /**
     * Gets the variables value for this Launch.
     * 
     * @return variables
     */
    public com.orsyp.duws.Variable[] getVariables() {
        return variables;
    }


    /**
     * Sets the variables value for this Launch.
     * 
     * @param variables
     */
    public void setVariables(com.orsyp.duws.Variable[] variables) {
        this.variables = variables;
    }


    /**
     * Gets the autoRestart value for this Launch.
     * 
     * @return autoRestart
     */
    public boolean isAutoRestart() {
        return autoRestart;
    }


    /**
     * Sets the autoRestart value for this Launch.
     * 
     * @param autoRestart
     */
    public void setAutoRestart(boolean autoRestart) {
        this.autoRestart = autoRestart;
    }


    /**
     * Gets the centralControl value for this Launch.
     * 
     * @return centralControl
     */
    public boolean isCentralControl() {
        return centralControl;
    }


    /**
     * Sets the centralControl value for this Launch.
     * 
     * @param centralControl
     */
    public void setCentralControl(boolean centralControl) {
        this.centralControl = centralControl;
    }


    /**
     * Gets the forcedExecution value for this Launch.
     * 
     * @return forcedExecution
     */
    public boolean isForcedExecution() {
        return forcedExecution;
    }


    /**
     * Sets the forcedExecution value for this Launch.
     * 
     * @param forcedExecution
     */
    public void setForcedExecution(boolean forcedExecution) {
        this.forcedExecution = forcedExecution;
    }


    /**
     * Gets the bypassCondCheck value for this Launch.
     * 
     * @return bypassCondCheck
     */
    public boolean isBypassCondCheck() {
        return bypassCondCheck;
    }


    /**
     * Sets the bypassCondCheck value for this Launch.
     * 
     * @param bypassCondCheck
     */
    public void setBypassCondCheck(boolean bypassCondCheck) {
        this.bypassCondCheck = bypassCondCheck;
    }


    /**
     * Gets the exclusionFrom value for this Launch.
     * 
     * @return exclusionFrom
     */
    public java.lang.String getExclusionFrom() {
        return exclusionFrom;
    }


    /**
     * Sets the exclusionFrom value for this Launch.
     * 
     * @param exclusionFrom
     */
    public void setExclusionFrom(java.lang.String exclusionFrom) {
        this.exclusionFrom = exclusionFrom;
    }


    /**
     * Gets the exclusionTo value for this Launch.
     * 
     * @return exclusionTo
     */
    public java.lang.String getExclusionTo() {
        return exclusionTo;
    }


    /**
     * Sets the exclusionTo value for this Launch.
     * 
     * @param exclusionTo
     */
    public void setExclusionTo(java.lang.String exclusionTo) {
        this.exclusionTo = exclusionTo;
    }


    /**
     * Gets the status value for this Launch.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Launch.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the beginDate value for this Launch.
     * 
     * @return beginDate
     */
    public java.lang.String getBeginDate() {
        return beginDate;
    }


    /**
     * Sets the beginDate value for this Launch.
     * 
     * @param beginDate
     */
    public void setBeginDate(java.lang.String beginDate) {
        this.beginDate = beginDate;
    }


    /**
     * Gets the beginHour value for this Launch.
     * 
     * @return beginHour
     */
    public java.lang.String getBeginHour() {
        return beginHour;
    }


    /**
     * Sets the beginHour value for this Launch.
     * 
     * @param beginHour
     */
    public void setBeginHour(java.lang.String beginHour) {
        this.beginHour = beginHour;
    }


    /**
     * Gets the endDate value for this Launch.
     * 
     * @return endDate
     */
    public java.lang.String getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this Launch.
     * 
     * @param endDate
     */
    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the endHour value for this Launch.
     * 
     * @return endHour
     */
    public java.lang.String getEndHour() {
        return endHour;
    }


    /**
     * Sets the endHour value for this Launch.
     * 
     * @param endHour
     */
    public void setEndHour(java.lang.String endHour) {
        this.endHour = endHour;
    }


    /**
     * Gets the processingDate value for this Launch.
     * 
     * @return processingDate
     */
    public java.lang.String getProcessingDate() {
        return processingDate;
    }


    /**
     * Sets the processingDate value for this Launch.
     * 
     * @param processingDate
     */
    public void setProcessingDate(java.lang.String processingDate) {
        this.processingDate = processingDate;
    }


    /**
     * Gets the user value for this Launch.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this Launch.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }


    /**
     * Gets the step value for this Launch.
     * 
     * @return step
     */
    public java.lang.String getStep() {
        return step;
    }


    /**
     * Sets the step value for this Launch.
     * 
     * @param step
     */
    public void setStep(java.lang.String step) {
        this.step = step;
    }


    /**
     * Gets the node value for this Launch.
     * 
     * @return node
     */
    public java.lang.String getNode() {
        return node;
    }


    /**
     * Sets the node value for this Launch.
     * 
     * @param node
     */
    public void setNode(java.lang.String node) {
        this.node = node;
    }


    /**
     * Gets the queue value for this Launch.
     * 
     * @return queue
     */
    public java.lang.String getQueue() {
        return queue;
    }


    /**
     * Sets the queue value for this Launch.
     * 
     * @param queue
     */
    public void setQueue(java.lang.String queue) {
        this.queue = queue;
    }


    /**
     * Gets the priority value for this Launch.
     * 
     * @return priority
     */
    public java.lang.String getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this Launch.
     * 
     * @param priority
     */
    public void setPriority(java.lang.String priority) {
        this.priority = priority;
    }


    /**
     * Gets the information value for this Launch.
     * 
     * @return information
     */
    public java.lang.String getInformation() {
        return information;
    }


    /**
     * Sets the information value for this Launch.
     * 
     * @param information
     */
    public void setInformation(java.lang.String information) {
        this.information = information;
    }


    /**
     * Gets the severity value for this Launch.
     * 
     * @return severity
     */
    public int getSeverity() {
        return severity;
    }


    /**
     * Sets the severity value for this Launch.
     * 
     * @param severity
     */
    public void setSeverity(int severity) {
        this.severity = severity;
    }


    /**
     * Gets the rankInSession value for this Launch.
     * 
     * @return rankInSession
     */
    public int getRankInSession() {
        return rankInSession;
    }


    /**
     * Sets the rankInSession value for this Launch.
     * 
     * @param rankInSession
     */
    public void setRankInSession(int rankInSession) {
        this.rankInSession = rankInSession;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Launch)) return false;
        Launch other = (Launch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ident==null && other.getIdent()==null) || 
             (this.ident!=null &&
              this.ident.equals(other.getIdent()))) &&
            ((this.variables==null && other.getVariables()==null) || 
             (this.variables!=null &&
              java.util.Arrays.equals(this.variables, other.getVariables()))) &&
            this.autoRestart == other.isAutoRestart() &&
            this.centralControl == other.isCentralControl() &&
            this.forcedExecution == other.isForcedExecution() &&
            this.bypassCondCheck == other.isBypassCondCheck() &&
            ((this.exclusionFrom==null && other.getExclusionFrom()==null) || 
             (this.exclusionFrom!=null &&
              this.exclusionFrom.equals(other.getExclusionFrom()))) &&
            ((this.exclusionTo==null && other.getExclusionTo()==null) || 
             (this.exclusionTo!=null &&
              this.exclusionTo.equals(other.getExclusionTo()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.beginDate==null && other.getBeginDate()==null) || 
             (this.beginDate!=null &&
              this.beginDate.equals(other.getBeginDate()))) &&
            ((this.beginHour==null && other.getBeginHour()==null) || 
             (this.beginHour!=null &&
              this.beginHour.equals(other.getBeginHour()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.endHour==null && other.getEndHour()==null) || 
             (this.endHour!=null &&
              this.endHour.equals(other.getEndHour()))) &&
            ((this.processingDate==null && other.getProcessingDate()==null) || 
             (this.processingDate!=null &&
              this.processingDate.equals(other.getProcessingDate()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.step==null && other.getStep()==null) || 
             (this.step!=null &&
              this.step.equals(other.getStep()))) &&
            ((this.node==null && other.getNode()==null) || 
             (this.node!=null &&
              this.node.equals(other.getNode()))) &&
            ((this.queue==null && other.getQueue()==null) || 
             (this.queue!=null &&
              this.queue.equals(other.getQueue()))) &&
            ((this.priority==null && other.getPriority()==null) || 
             (this.priority!=null &&
              this.priority.equals(other.getPriority()))) &&
            ((this.information==null && other.getInformation()==null) || 
             (this.information!=null &&
              this.information.equals(other.getInformation()))) &&
            this.severity == other.getSeverity() &&
            this.rankInSession == other.getRankInSession();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdent() != null) {
            _hashCode += getIdent().hashCode();
        }
        if (getVariables() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVariables());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVariables(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isAutoRestart() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isCentralControl() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isForcedExecution() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isBypassCondCheck() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getExclusionFrom() != null) {
            _hashCode += getExclusionFrom().hashCode();
        }
        if (getExclusionTo() != null) {
            _hashCode += getExclusionTo().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getBeginDate() != null) {
            _hashCode += getBeginDate().hashCode();
        }
        if (getBeginHour() != null) {
            _hashCode += getBeginHour().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getEndHour() != null) {
            _hashCode += getEndHour().hashCode();
        }
        if (getProcessingDate() != null) {
            _hashCode += getProcessingDate().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getStep() != null) {
            _hashCode += getStep().hashCode();
        }
        if (getNode() != null) {
            _hashCode += getNode().hashCode();
        }
        if (getQueue() != null) {
            _hashCode += getQueue().hashCode();
        }
        if (getPriority() != null) {
            _hashCode += getPriority().hashCode();
        }
        if (getInformation() != null) {
            _hashCode += getInformation().hashCode();
        }
        _hashCode += getSeverity();
        _hashCode += getRankInSession();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Launch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ident");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "ident"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("variables");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "variables"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "variable"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "variables"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autoRestart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "autoRestart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("centralControl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "centralControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forcedExecution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "forcedExecution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bypassCondCheck");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "bypassCondCheck"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exclusionFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "exclusionFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exclusionTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "exclusionTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beginHour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "beginHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endHour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "endHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processingDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "processingDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("step");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "step"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("node");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "node"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "queue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("information");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "information"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("severity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "severity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankInSession");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "rankInSession"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

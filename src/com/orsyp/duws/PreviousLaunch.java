/**
 * PreviousLaunch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class PreviousLaunch  implements java.io.Serializable {
    private java.lang.String uproc;

    private java.lang.String numProc;

    private java.lang.String launchDate;

    private java.lang.String launchHour;

    private java.lang.String type;

    public PreviousLaunch() {
    }

    public PreviousLaunch(
           java.lang.String uproc,
           java.lang.String numProc,
           java.lang.String launchDate,
           java.lang.String launchHour,
           java.lang.String type) {
           this.uproc = uproc;
           this.numProc = numProc;
           this.launchDate = launchDate;
           this.launchHour = launchHour;
           this.type = type;
    }


    /**
     * Gets the uproc value for this PreviousLaunch.
     * 
     * @return uproc
     */
    public java.lang.String getUproc() {
        return uproc;
    }


    /**
     * Sets the uproc value for this PreviousLaunch.
     * 
     * @param uproc
     */
    public void setUproc(java.lang.String uproc) {
        this.uproc = uproc;
    }


    /**
     * Gets the numProc value for this PreviousLaunch.
     * 
     * @return numProc
     */
    public java.lang.String getNumProc() {
        return numProc;
    }


    /**
     * Sets the numProc value for this PreviousLaunch.
     * 
     * @param numProc
     */
    public void setNumProc(java.lang.String numProc) {
        this.numProc = numProc;
    }


    /**
     * Gets the launchDate value for this PreviousLaunch.
     * 
     * @return launchDate
     */
    public java.lang.String getLaunchDate() {
        return launchDate;
    }


    /**
     * Sets the launchDate value for this PreviousLaunch.
     * 
     * @param launchDate
     */
    public void setLaunchDate(java.lang.String launchDate) {
        this.launchDate = launchDate;
    }


    /**
     * Gets the launchHour value for this PreviousLaunch.
     * 
     * @return launchHour
     */
    public java.lang.String getLaunchHour() {
        return launchHour;
    }


    /**
     * Sets the launchHour value for this PreviousLaunch.
     * 
     * @param launchHour
     */
    public void setLaunchHour(java.lang.String launchHour) {
        this.launchHour = launchHour;
    }


    /**
     * Gets the type value for this PreviousLaunch.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this PreviousLaunch.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PreviousLaunch)) return false;
        PreviousLaunch other = (PreviousLaunch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uproc==null && other.getUproc()==null) || 
             (this.uproc!=null &&
              this.uproc.equals(other.getUproc()))) &&
            ((this.numProc==null && other.getNumProc()==null) || 
             (this.numProc!=null &&
              this.numProc.equals(other.getNumProc()))) &&
            ((this.launchDate==null && other.getLaunchDate()==null) || 
             (this.launchDate!=null &&
              this.launchDate.equals(other.getLaunchDate()))) &&
            ((this.launchHour==null && other.getLaunchHour()==null) || 
             (this.launchHour!=null &&
              this.launchHour.equals(other.getLaunchHour()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUproc() != null) {
            _hashCode += getUproc().hashCode();
        }
        if (getNumProc() != null) {
            _hashCode += getNumProc().hashCode();
        }
        if (getLaunchDate() != null) {
            _hashCode += getLaunchDate().hashCode();
        }
        if (getLaunchHour() != null) {
            _hashCode += getLaunchHour().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PreviousLaunch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "previousLaunch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uproc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uproc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numProc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "numProc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("launchDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("launchHour");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "launchHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}

/**
 * UvmsContext.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.orsyp.duws;

public class UvmsContext  implements java.io.Serializable {
    private java.lang.String uvmsHost;

    private int uvmsPort;

    private java.lang.String uvmsUser;

    private java.lang.String uvmsPassword;

    public UvmsContext() {
    }

    public UvmsContext(
           java.lang.String uvmsHost,
           int uvmsPort,
           java.lang.String uvmsUser,
           java.lang.String uvmsPassword) {
           this.uvmsHost = uvmsHost;
           this.uvmsPort = uvmsPort;
           this.uvmsUser = uvmsUser;
           this.uvmsPassword = uvmsPassword;
    }


    /**
     * Gets the uvmsHost value for this UvmsContext.
     * 
     * @return uvmsHost
     */
    public java.lang.String getUvmsHost() {
        return uvmsHost;
    }


    /**
     * Sets the uvmsHost value for this UvmsContext.
     * 
     * @param uvmsHost
     */
    public void setUvmsHost(java.lang.String uvmsHost) {
        this.uvmsHost = uvmsHost;
    }


    /**
     * Gets the uvmsPort value for this UvmsContext.
     * 
     * @return uvmsPort
     */
    public int getUvmsPort() {
        return uvmsPort;
    }


    /**
     * Sets the uvmsPort value for this UvmsContext.
     * 
     * @param uvmsPort
     */
    public void setUvmsPort(int uvmsPort) {
        this.uvmsPort = uvmsPort;
    }


    /**
     * Gets the uvmsUser value for this UvmsContext.
     * 
     * @return uvmsUser
     */
    public java.lang.String getUvmsUser() {
        return uvmsUser;
    }


    /**
     * Sets the uvmsUser value for this UvmsContext.
     * 
     * @param uvmsUser
     */
    public void setUvmsUser(java.lang.String uvmsUser) {
        this.uvmsUser = uvmsUser;
    }


    /**
     * Gets the uvmsPassword value for this UvmsContext.
     * 
     * @return uvmsPassword
     */
    public java.lang.String getUvmsPassword() {
        return uvmsPassword;
    }


    /**
     * Sets the uvmsPassword value for this UvmsContext.
     * 
     * @param uvmsPassword
     */
    public void setUvmsPassword(java.lang.String uvmsPassword) {
        this.uvmsPassword = uvmsPassword;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UvmsContext)) return false;
        UvmsContext other = (UvmsContext) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uvmsHost==null && other.getUvmsHost()==null) || 
             (this.uvmsHost!=null &&
              this.uvmsHost.equals(other.getUvmsHost()))) &&
            this.uvmsPort == other.getUvmsPort() &&
            ((this.uvmsUser==null && other.getUvmsUser()==null) || 
             (this.uvmsUser!=null &&
              this.uvmsUser.equals(other.getUvmsUser()))) &&
            ((this.uvmsPassword==null && other.getUvmsPassword()==null) || 
             (this.uvmsPassword!=null &&
              this.uvmsPassword.equals(other.getUvmsPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUvmsHost() != null) {
            _hashCode += getUvmsHost().hashCode();
        }
        _hashCode += getUvmsPort();
        if (getUvmsUser() != null) {
            _hashCode += getUvmsUser().hashCode();
        }
        if (getUvmsPassword() != null) {
            _hashCode += getUvmsPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UvmsContext.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsContext"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uvmsHost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uvmsPort");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsPort"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uvmsUser");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uvmsPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("http://duws.orsyp.com", "uvmsPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
